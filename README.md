## cpp Graph Generator and Visualizer
<img src="media/screenshot1.png" alt="PathFinding" width="640" height="360">
<img src="media/screenshot2.png" alt="Communities" width="640" height="360">
<img src="media/screenshot3.png" alt="PageRank" width="640" height="360">

This is a project I made out of curiosity. It is a graph visualizer built in c++ and using SFML 2 for linux.

I made it quick, so it doesn't follow perfectly best practices, but it is still readable.

It runs natively in 1920*1080. You can choose your own resolution, see the last section of the manual.

It is meant to be used on small graphs ( < 50 ), because the drawing algorithm used is a simple force-directed spring.
Which doesn't have good results on large graphs. Using a suited algorithm for large graphs is on the todo list.

You can load your own graph files, but the project also includes a basic graph generator and some test graphs named "tmp1"->"tmp7".

It also displays a few algorithms:
- A Pathfinding animation that uses Breadth First Search, between a chosen starting point and target.
- A simple Communities algorithm with a colored display.
- A simple PageRank algorithm, that will highlight the node with the highest pagerank score.

You can navigate within the canvas using the mousewheel button and drag.
Also, you can use the mousewheel to zoom in and zoom out.

It has few more features, see the manual for more informations.

### Setup
To compile and to run you need to have the SFML 2 installed on your computer. 
For example, you can do so on a Ubuntu-based system using the command :

`sudo apt install libsfml-dev`

This project has been done using sfml 2.5.1, but an earlier version should work fine. This is the only prerequisite.

### Manual
Movement:
- Use the mousewheel button to move the graphs around.
- Use the mousewheel to zoom in and zoom out.
- Use ctrl+left-click to zoom in on a selection.

Using Generator:
- The number of nodes cannot be < 2.
- The cluster size cannot be < 1.
- The linking probabilities cannot be > 1 nor < 0.

Loading a graph from a file:
- Your file must be formatted like the examples in the 'tests' directory
- Put your file in the 'tests' directory.
- Use the text prompt and the button to load your file.
- WARNING: Right now there is no reindexing, so your node labels must start from 0, and no label should be skipped.

PathFinding:
- Computes the shortest path between a target and a starting node using a Breadth-First Search.
- Use the left mouse button to choose the target.
- Use the right mouse button to choose the starting node.
- Can be used with directed and undirected graphs.

Communities:
-  Computes the communities of the graph and displays them with colors.
-  Can be used with directed and undirected graphs.

PageRank:
- Computes the node with the highest PageRank, and gives it color.
- This is meant to be used with directed graphs, still, you can use it on undirected graphs.

Miscellaneous:
- Use the time step prompt to choose the path finding animation speed.
- You can reset the view using the Reset View button.

Use of make:
- make : compiles the project.
- make clean : removes all .o files from the 'obj' folder.
- make cleanall : removes all .o files from the 'obj' folder and also
the executable.
- make run : runs the program.
- make run-d : runs the program in dark mode.
- make man : shows you this manual ( but with colors ).

Choosing your resolution:
- The program runs natively in 1920*1080. You can choose your own resolution like so: `make run W=1280 H=720`

### Notes of author
If you have any questions about this project you can write to me at: gabriel.jeantet@gmail.com (this is an email that I don't use much, but I'll answer !).

This project is under the MIT license so have fun with it.

And if you reuse it for any project, show me! I would be happy to see 🙂

GJ.



