# ------------------------------------------------
# Makefile
#
# Author: Gabriel Jeantet
# Email: gabriel.jeantetpro@gmail.com
# Date  : 04-03-2020
#
# ------------------------------------------------


CC=g++
CFLAGS := -g
LFLAGS := -lsfml-graphics -lsfml-window -lsfml-system

SRCDIR := src
BINDIR := bin
OBJDIR := obj
TARGET := $(BINDIR)/graphs

DARK_MODE := 1
LIGHT_MODE := 0

W := 1920
H := 1080

SRC_LIST := $(wildcard $(SRCDIR)/*.cpp)
OBJ_LIST := $(SRC_LIST:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

# pretty colors
RED=\033[0;31m
BLUE=\033[0;34m
GREEN=\033[0;32m
NOCOLOR=\033[0m

all: $(TARGET)


$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@echo "$(GREEN)Compiling$(NOCOLOR) $(BLUE)$@$(NOCOLOR)"
	$(CC) $(CFLAGS) -c $< -o $@

$(TARGET): $(OBJ_LIST)
	@echo "$(GREEN)Linking$(NOCOLOR) $(BLUE)$@$(NOCOLOR)"
	$(CC) -o $@ $^ $(LFLAGS)
	@echo "$(GREEN)Linking complete$(NOCOLOR)"
	@echo "$(GREEN)binary$(NOCOLOR) $(BLUE)$@$(NOCOLOR) $(GREEN)available in$(NOCOLOR) $(BLUE)$(BINDIR)$(NOCOLOR) $(GREEN)directory$(NOCOLOR)"



.PHONY: clean

clean:
	@echo "$(RED)Cleaning$(NOCOLOR) $(BLUE)$(OBJDIR)$(NOCOLOR) $(RED)directory$(NOCOLOR)"
	rm -f $(OBJDIR)/*.o

cleanall: clean remove


remove:
	@echo "$(RED)Removing$(NOCOLOR) $(BLUE)$(TARGET)$(NOCOLOR)"
	rm -f $(TARGET)

run: $(TARGET)
	@$(TARGET) $(W) $(H) $(LIGHT_MODE)

run-d: $(TARGET)
	@$(TARGET) $(W) $(H) $(DARK_MODE)

man:
	@echo "$(BLUE)************************************ MANUAL ************************************"
	@echo "Movement:$(NOCOLOR)"
	@echo "\t $(GREEN)-Use the mousewheel button to move the graphs around."
	@echo "\t -Use the mousewheel to zoom in and zoom out."
	@echo "\t -Use ctrl+left-click to zoom in on a selection. $(NOCOLOR)"
	@echo "$(BLUE)Using Generator:$(NOCOLOR)"
	@echo "\t $(GREEN)-The number of nodes cannot be < 2."
	@echo "\t -The cluster size cannot be < 1."
	@echo "\t -The linking probabilities cannot be > 1 nor < 0$(NOCOLOR)."
	@echo "$(BLUE)Loading a graph from a file:$(NOCOLOR)"
	@echo "\t $(GREEN)-Your file must be formatted like the examples in the $(NOCOLOR)'tests'$(GREEN) directory"
	@echo "\t -Put your file in the $(NOCOLOR)'tests'$(GREEN) directory."
	@echo "\t -Use the text prompt and the button to load your file.$(NOCOLOR)"
	@echo "\t $(RED)-WARNING:$(NOCOLOR) $(GREEN)Right now there is no reindexing, so your node labels must"
	@echo "\t start from 0, and no label should be skipped.$(NOCOLOR)"
	@echo "$(BLUE)PathFinding:$(NOCOLOR)"
	@echo "\t $(GREEN)-Computes the shortest path between a target and a starting node"
	@echo "\t using a Breadth First Search."
	@echo "\t -Use the left mouse button to choose the target."
	@echo "\t -Use the right mouse button to choose the starting node."
	@echo "\t -Can be used with directed and undirected graphs.$(NOCOLOR)"
	@echo "$(BLUE)Communities:$(NOCOLOR)"
	@echo "\t $(GREEN)-Computes the communities of the graph and displays them with colors."
	@echo "\t -Can be used with directed and undirected graphs.$(NOCOLOR)"
	@echo "$(BLUE)PageRank:$(NOCOLOR)"
	@echo "\t $(GREEN)-Computes the node with the highest PageRank, and gives it a color."
	@echo "\t -This is meant to be used with directed graphs, still you can use it"
	@echo "\t on undirected graphs.$(NOCOLOR)"
	@echo "$(BLUE)Miscellaneous:$(NOCOLOR)"
	@echo "\t $(GREEN)-Use the time step prompt to choose the path finding animation speed."
	@echo "\t -You can reset the view using the Reset View button.$(NOCOLOR)"
	@echo "$(BLUE)Use of make:$(NOCOLOR)"
	@echo "\t -make $(GREEN): compiles the project."
	@echo "\t $(NOCOLOR)-make clean $(GREEN): removes all .o files from the 'obj' folder."
	@echo "\t $(NOCOLOR)-make cleanall $(GREEN): removes all .o files from the 'obj' folder and also"
	@echo "\t the executable."
	@echo "\t $(NOCOLOR)-make run $(GREEN): runs the program."
	@echo "\t $(NOCOLOR)-make run-d $(GREEN): runs the program in dark mode."
	@echo "\t $(NOCOLOR)-make man $(GREEN): shows you this message.$(NOCOLOR)"
	@echo "$(BLUE)Choosing your resolution:$(NOCOLOR)"
	@echo "\t $(GREEN)The program runs natively in 1920*1080. You can choose your own"
	@echo "\t resolution like so :$(NOCOLOR) make run W=1280 H=720"
