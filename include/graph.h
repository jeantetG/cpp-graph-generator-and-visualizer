// ------------------------------------------------
// graph.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#ifndef GRAPHFONC_H_
#define GRAPHFONC_H_

#include <string>
#include <cstring>
#include <iostream>
#include <cstdio>
#include <map>
#include <SFML/Graphics.hpp>

#include "FIFO.h"
#include "edge.h"
#include "2dGeometry.h"
#include "global_vars.h"

using namespace std;

enum NodeType {NONE = 0,TARGET, START};


unsigned long max3(unsigned long a,unsigned long b,unsigned long c);


class EdgeList{
public:
	unsigned long n; // nodes number
	unsigned long e; //edges number
	Edge *elist;
	Point *positions;
	NodeType *node_type_list;
	unsigned int target;
	unsigned int start;
	unsigned int* dist_to_origin;
	int* predecessor;
	bool pathProcessed;
	int shortest_path_length;
	float delta_x;
	float delta_y;
	float scale;
	float tenth_scale;
	float o_scale;
	float o_width;
	float o_height;
	bool is_directed;
	unsigned int* labels;
	bool communities_processed;
	std::map<int, sf::Color> labels_colors;
	bool pagerank_processed;
	unsigned long highest_pagerank;

	EdgeList();
	~EdgeList();
	void loadEdgeList(string p);
	
};


class AdjArray : public EdgeList{
public:
	unsigned long *cd; //cumulative degree cd[0]=0 length=n+1
	unsigned long *adj;//concatenated lists of neighbors of all nodes
	unsigned long *rcd; //cumulative reverse degree cd[0]=0 length=n+1
	unsigned long *radj;//concatenated list fo fathers of all nodes


	AdjArray();
	~AdjArray();
	void loadAdjArrayUndirected(string p);
	void loadAdjArrayDirected(string p);
	void printAdj();
	bool areNeighboursUndirected(unsigned long n1,unsigned long n2);
	

};


#endif /* GRAPHFONC_H_ */
