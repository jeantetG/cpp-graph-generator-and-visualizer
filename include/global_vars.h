// ------------------------------------------------
// global_vars.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once
#include <SFML/Graphics.hpp>

// std window width
extern float STD_WINDOW_WIDTH;
//gui size
extern float WINDOW_WIDTH;
extern float WINDOW_HEIGHT;
extern int NB_BUTTONS;
extern float BUTTON_HEIGHT;
extern float BUTTON_WIDTH ;
extern float CANVAS_WIDTH ;
extern float CANVAS_HEIGHT ;
extern float TEXT_SIZE;
extern float SELECTION_OUTLINE_WIDTH;
// fifo max size
extern long NLINKS;
// animation speed
extern int STEP; // in milliseconds
// graph drawing variables
extern float STD_NODE_SIZE_PX;
extern float STD_VERTEX_SIZE_PX;
extern float BASE_STD_NODE_SIZE_PX;
extern float BASE_STD_VERTEX_SIZE_PX;
extern int MAX_N_NODES_WITHOUT_SCALING;
extern float MAX_SURFACE_WITHOUT_SCALING;
extern float MIN_NODE_SIZE;
extern float MIN_VERTEX_SIZE;
extern float MAX_ZOOM;
// spring constants
extern float C1;
extern float C2;
extern float C3;
extern float C4;
// Page rank variables
extern int MAX_ITER;
extern float BETA;
extern float PRECISION;
// GUI COLORS
extern sf::Color BLUE;
extern sf::Color GREY;
extern sf::Color DARK_GREY;
extern bool dark_mode_on;

