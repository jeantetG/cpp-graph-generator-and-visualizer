// ------------------------------------------------
// graphDrawingAlgorithms.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once

#include <SFML/Graphics.hpp>
#include <cstdlib>
#include "graph.h"
#include "graphDrawing.h"
#include <vector>
#include "global_vars.h"
#include "2dGeometry.h"

void springDrawGenerate(float width, float height, AdjArray * graph, int max_iteration);
void randomDrawGenerate(float width, float height, AdjArray * graph);

class VertexArgs{
    public:
        float x1;
        float x2;
        float y1;
        float y2;

        VertexArgs(float x1arg, float y1arg, float x2arg, float y2arg):x1(x1arg),y1(y1arg),x2(x2arg),y2(y2arg){}
};

class WGarg{
    public:
        vector<VertexArgs>* animations_vertex;
        vector<sf::CircleShape>* animations_node;
        AdjArray* graph;

        WGarg(vector<sf::CircleShape>* an,vector<VertexArgs>* av,AdjArray* g);
};

