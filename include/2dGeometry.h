// ------------------------------------------------
// 2dGeometry.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once
#include <cmath>

class Vector{
    private:

        float vx;
        float vy;
    
    public:

        Vector(float vxarg, float vyarg);
        Vector();
        void translate(Vector v);
        void setVX(float vxarg);
        void setVY(float vyarg);
        float getVX();
        float getVY();
        Vector operator*(float k);
        Vector operator/(float k);
        float getNorm();
};


class Point{
    private:
    
        float x;
        float y;
    
    public:

        Point(float xarg, float yarg);
        Point();
        void translate(Vector v);
        void setX(float xarg);
        void setY(float yarg);
        float getX();
        float getY();
};



float distance(Point a, Point b);

Vector vectorDirector(Point a, Point b);