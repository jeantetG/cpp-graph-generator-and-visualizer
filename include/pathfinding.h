// ------------------------------------------------
// pathfinding.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once

#include "graph.h"
#include "graphDrawingAlgorithms.h"
#include "global_vars.h"
#include <unistd.h>


bool leftNodeClick(float mx, float my, float originx, float originy, AdjArray* graph);

bool rightNodeClick(float mx, float my, float originx, float originy, AdjArray* graph);

void shortestPathBFS(WGarg* wgarg);