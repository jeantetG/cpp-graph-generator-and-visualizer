// ------------------------------------------------
// graphDrawing.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once

#include "../include/graphDrawingAlgorithms.h"
#include "graph.h"

void drawGraph(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph);
void drawGraphPathFinding(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph);
void drawGraphCommunities(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph);
void drawGraphPageRank(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph);
sf::RectangleShape createLine(float x1,float y1, float x2, float y2, float width, sf::Color color);
void printPositions(AdjArray * graph);
float morphToScaleX(float f ,AdjArray* graph);
float morphToScaleY(float f ,AdjArray* graph);
void drawArrowHead(float x1,float y1, float x2, float y2, float width, sf::Color color,sf::RenderWindow * window, AdjArray* graph);