// ------------------------------------------------
// gui.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include "graphDrawingAlgorithms.h"
#include "pathfinding.h"
#include "graph_generator.h"

using namespace std;

enum ActiveSceneType { PATH_FINDING, COMMUNITIES, PAGERANK};


class Button {
    private:

        sf::RenderWindow * renderWindow;
        sf::RectangleShape * box;
        sf::Text * text;
        sf::Font * font;       
    
    public:
        void (*callback)(void*);

        Button(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string text, int textSize, void (*cb)(void*));
        ~Button();
        sf::RectangleShape * getBox();
        void drawButton();
        bool clicked(float x, float y);
        void ifClickedCallBack(void* b, float x, float y);
};
class CheckBox {
    private:

        sf::RenderWindow * renderWindow;
        sf::RectangleShape * box;
        sf::Text * text;
        sf::Font * font;       
    
    public:
        bool checked;

        CheckBox(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string text, int textSize, bool c );
        ~CheckBox();
        void drawCheckBox();
        bool clicked(float x, float y);
        void flip();
        bool getPayload();
};

class TextBox {
    private:
        sf::RenderWindow * renderWindow;
        sf::RectangleShape * box;
        sf::Text * text;
        sf::Font * font;
        sf::Text * textPrompt;
        sf::Text * textPromptTmp;
        sf::RectangleShape * textbox;
        bool is_selected;
        float _w;
        float _h;
        float _px;
        float _py;
    
    public:
        TextBox(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string text, int textSize, string default_prompt);
        ~TextBox();
        sf::RectangleShape * getBox();
        void drawTextBox();
        bool clicked(float x, float y);
        void setTextPrompt(string t);
        void setSelected(bool b);
        void setTextPromptTmp(string t);
        sf::Text* getTextPromptTmp();
        sf::Text* getTextPrompt();    
        float getH();
        float getW();
        float getPX();
        float getPY();      
              
};

class TextBoxInt : public TextBox{
    private:
        int payload;
    public:
        TextBoxInt(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string text, int textSize, int pl): TextBox{w, h, px, py, rw, fontPath, text, textSize, to_string(pl)}, payload(pl){};
        void setPayload(int i);
        int getPayload();
};

class TextBoxFloat : public TextBox{
    private:
        double payload;
    public:
        TextBoxFloat(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string text, int textSize, float pl): TextBox{w, h, px, py, rw, fontPath, text, textSize, to_string(pl)}, payload(pl){};
        void setPayload(float i);
        float getPayload();
};

class TextBoxString : public TextBox{
    private:
        string payload;
    public:
        TextBoxString(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string text, int textSize, string pl): TextBox{w, h, px, py, rw, fontPath, text, textSize, pl}, payload(pl){};
        void setPayload(string i);
        string getPayload();
};

void switch_to_pathfinding(void * b);

void switch_to_communities(void * b);

void switch_to_pagerank(void * b);

void startBFS(void *b);

void loadGeneratedGraph(void *b);

void resetVIEW(void *b);
