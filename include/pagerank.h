// ------------------------------------------------
// pagerank.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once

#include "graph.h"
#include "global_vars.h"

void processPageRank(AdjArray* graph);