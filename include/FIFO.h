// ------------------------------------------------
// FIFO.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#ifndef FIFO_H_
#define FIFO_H_

#include <iostream>

class FIFO{

private:
	int* fifo;
	int capacity;

	int first;
	int last;
	int sz;


public:	

	FIFO(int cap);
	~FIFO();
	int pop();
	void push(int t);
	bool isEmpty();
	bool isFull();
	int size();

};



#endif /* FIFO_H_ */

