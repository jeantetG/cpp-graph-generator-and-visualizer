// ------------------------------------------------
// communities.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once

#include "graph.h"
#include <vector>
#include <algorithm>

void processing_communities(AdjArray* graph);
