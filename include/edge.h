// ------------------------------------------------
// edge.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once

class Edge{

public:
	unsigned long n1;
	unsigned long n2;


	Edge(unsigned long a1,unsigned long a2);
};
