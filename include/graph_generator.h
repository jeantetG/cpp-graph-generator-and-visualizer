// ------------------------------------------------
// graph_generator.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#pragma once
#include <string>
#include <iostream>

using namespace std;

void generate_graph_undirected(int n, int cluster_size, float p_cluster, float p_other);
void generate_graph_directed(int n, int cluster_size, float p_cluster, float p_other);
float rand_Proba();