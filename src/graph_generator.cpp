// ------------------------------------------------
// graph_generator.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/graph_generator.h"

void generate_graph_undirected(int n, int cluster_size, float p_cluster, float p_other){
    string args = "w";
    
    FILE *file = fopen("./tests/generated_graph",args.c_str());

    if (file == NULL) {
        cerr << "Couldn't load file" << endl;
        exit(1);
    }
    for(int i = 0; i < n ; i ++ ){
        for(int j = i+1; j < n; j++){
            if( cluster_size!=1 && (j/cluster_size == i/cluster_size )){
                if(rand_Proba()<=p_cluster){
                    fprintf(file,"%d %d\n",i,j);
                }
            }else{
                if(rand_Proba()<=p_other){
                    
                    fprintf(file,"%d %d\n",i,j);
                }
            }
        }
    }

    fclose(file);
}

void generate_graph_directed(int n, int cluster_size, float p_cluster, float p_other){
    string args = "w";
    
    FILE *file = fopen("./tests/generated_graph",args.c_str());

    if (file == NULL) {
        cerr << "Couldn't load file" << endl;
        exit(1);
    }
    for(int i = 0; i < n ; i ++ ){
        for(int j = 0; j < n; j++){
            if(i==j){
                continue;
            }
            if( cluster_size!=1 && (j/cluster_size == i/cluster_size )){
                if(rand_Proba()<=p_cluster){
                    fprintf(file,"%d %d\n",i,j);
                }
            }else{
                if(rand_Proba()<=p_other){
                    
                    fprintf(file,"%d %d\n",i,j);
                }
            }
        }
    }

    fclose(file);
}


float rand_Proba(){
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}