// ------------------------------------------------
// graph.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/graph.h"

unsigned long max3(unsigned long a,unsigned long b,unsigned long c){
	a = (a > b) ? a : b;
	return (a > c) ? a : c;
}

EdgeList::EdgeList(){
    e = 0;
    n = 0;
    elist = (Edge*)malloc(NLINKS*sizeof(Edge));
    if(elist == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
}
void EdgeList::loadEdgeList(string p){
    string args = "r";
    //string args2 = "w";
    unsigned long max=NLINKS;
    
    FILE *file = fopen(p.c_str(),args.c_str());
    if (file == NULL) {
        cerr << "Couldn't load file" << endl;
        exit(1);
    }
    unsigned long a;
    unsigned long b;

    while(fscanf(file,"%lu %lu",&a,&b)==2){
        elist[e].n1=a;
        elist[e].n2=b;
        n = max3(n,elist[e].n1,elist[e].n2);
        e++;
        if(e==max){
            max=max+NLINKS;
            elist = (Edge*)realloc(elist, max*sizeof(Edge));
            if(elist == NULL){
                cerr << "couldn't allocate memory, aborting" << endl;
                exit(1);
            }
        }
    }
    fclose(file);

    n++;
    
    positions = (Point*)malloc(sizeof(Point)*n);
    if(positions == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    node_type_list = (NodeType*)calloc(n,sizeof(NodeType));
    if(node_type_list == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    dist_to_origin = (unsigned int *)malloc(n*sizeof(unsigned int));
    if(dist_to_origin == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    predecessor = (int *)malloc(n*sizeof(int));
    if(predecessor == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    pathProcessed = false;
    target = -1;
    start = -1;
    delta_x = 0;
    delta_y = 0;
    scale = 1;
    tenth_scale = 0.1;
    o_scale = 1;
    communities_processed=false;
    pagerank_processed=false;
    labels = (unsigned int *)malloc(n*sizeof(unsigned int));
    if(labels == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    elist = (Edge*)realloc(elist,e*sizeof(Edge));
    if(elist == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }

}
EdgeList::~EdgeList(){
    free(elist);
    free(node_type_list);
    free(dist_to_origin);
    free(predecessor);
    free(positions);
    free(labels);
}




AdjArray::AdjArray(){
    e = 0;
    n = 0;
}

void AdjArray::loadAdjArrayUndirected(string p){
  
    this->loadEdgeList(p);   

    unsigned long *d = (unsigned long *)calloc(n,sizeof(unsigned long));
    if(d == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    unsigned long *rd = (unsigned long *)calloc(n,sizeof(unsigned long));
    if(rd == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    cd = (unsigned long *)calloc(n+1,sizeof(unsigned long));
    if(cd == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    rcd = (unsigned long *)calloc(n+1,sizeof(unsigned long));
    if(rcd == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }

    for(unsigned long i = 0 ; i<e ; i++ ){
        d[elist[i].n1]++;
        d[elist[i].n2]++;
        rd[elist[i].n1]++;
        rd[elist[i].n2]++;
    }
    for(unsigned long i = 1; i<=n ; i++){
        cd[i] = cd[i-1]+d[i-1];
        d[i-1]=0;
        rcd[i] = rcd[i-1]+rd[i-1];
        rd[i-1]=0;
    }

    unsigned long u,v;

    adj = (unsigned long *)malloc(2*e*sizeof(unsigned long));
    if(adj == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    radj = (unsigned long *)malloc(2*e*sizeof(unsigned long));
    if(radj == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    for(unsigned long i = 0 ; i<e ; i++ ){
        u=elist[i].n1;
        v=elist[i].n2;
        adj[ cd[u] + d[u]++ ]=v;
        adj[ cd[v] + d[v]++ ]=u;
        radj[ rcd[u] + rd[u]++ ]=v;
        radj[ rcd[v] + rd[v]++ ]=u;
    }

    is_directed=false;

    free(d);
    free(rd);
}
void AdjArray::loadAdjArrayDirected(string p){
  
    this->loadEdgeList(p);   

    unsigned long *d = (unsigned long *)calloc(n,sizeof(unsigned long));
    if(d == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    unsigned long *rd = (unsigned long *)calloc(n,sizeof(unsigned long));
    if(rd == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    cd = (unsigned long *)calloc(n+1,sizeof(unsigned long));
    if(cd == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    rcd = (unsigned long *)calloc(n+1,sizeof(unsigned long));
    if(rcd == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }

    for(unsigned long i = 0 ; i<e ; i++ ){
        d[elist[i].n1]++;
        rd[elist[i].n2]++;
    }
    for(unsigned long i = 1; i<=n ; i++){
        cd[i] = cd[i-1]+d[i-1];
        d[i-1]=0;
        rcd[i] = rcd[i-1]+rd[i-1];
        rd[i-1]=0;
    }

    unsigned long u,v;

    adj = (unsigned long *)malloc(2*e*sizeof(unsigned long));
    if(adj == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    radj = (unsigned long *)malloc(2*e*sizeof(unsigned long));
    if(radj == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    for(unsigned long i = 0 ; i<e ; i++ ){
        u=elist[i].n1;
        v=elist[i].n2;
        adj[ cd[u] + d[u]++ ]=v;
        radj[ rcd[v] + rd[v]++ ]=u;
    }

    is_directed=true;

    free(d);
    free(rd);
}

AdjArray::~AdjArray(){

    free(cd);
    free(adj);
    free(rcd);
    free(radj);
    
    cd=nullptr;
    adj=nullptr;
    rcd=nullptr;
    radj=nullptr;
}


void AdjArray::printAdj(){
    AdjArray* k = this;
	for(unsigned long j =0; j < k->n;j++){
		cout << j << " neigbhours are : " ;
		for(unsigned long i=k->cd[j]; i < k->cd[j+1] ;i++){
			cout << k->adj[i]<< "  ";

		}
		cout << endl;
	}
	cout << endl;
	cout << endl;
}

bool AdjArray::areNeighboursUndirected(unsigned long n1,unsigned long n2){
    for(unsigned long i=this->cd[n1]; i < this->cd[n1+1] ;i++){
        if(this->adj[i] == n2){
            return true;
        }
    }
    return false;
}