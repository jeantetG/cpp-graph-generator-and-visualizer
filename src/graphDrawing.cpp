// ------------------------------------------------
// graphDrawing.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/graphDrawing.h"
#include <unistd.h>
/*
void drawGraph(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph){
    //drawing lines
    for(int i = 0; i < graph->e ; i++){
        //getting starting and ending points
        float a_x = morphToScaleX(graph->positions[graph->elist[i].n1].getX() + STD_NODE_SIZE_PX,graph) + origin_x ;
        float a_y = morphToScaleY(graph->positions[graph->elist[i].n1].getY() + STD_NODE_SIZE_PX,graph) + origin_y ;
        float b_x = morphToScaleX(graph->positions[graph->elist[i].n2].getX() + STD_NODE_SIZE_PX,graph) + origin_x ;
        float b_y = morphToScaleY(graph->positions[graph->elist[i].n2].getY() + STD_NODE_SIZE_PX,graph) + origin_y ;
        
        sf::RectangleShape tmp = createLine(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX*graph->scale,sf::Color(0, 0, 0));
        tmp.move(graph->delta_x, graph->delta_y);
        window->draw(tmp);
        if(graph->is_directed){            
            drawArrowHead(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX*graph->scale,sf::Color(0,0,0),window,graph);
        }
    }
    //drawing nodes
    for(int i = 0 ; i < graph->n ; i ++){
        //create circleshape for node
        sf::CircleShape node(STD_NODE_SIZE_PX*graph->scale);
        //sf::CircleShape node(1);
        
        node.setFillColor(sf::Color(0, 0, 255));
        
        
        node.setPosition(morphToScaleX(graph->positions[i].getX(), graph)+origin_x,morphToScaleY(graph->positions[i].getY(),graph)+origin_y);
        node.move(graph->delta_x, graph->delta_y);
        //drawing node
        window->draw(node);
    }  
}*/

void drawGraphPathFinding(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph){
    //drawing lines
    for(int i = 0; i < graph->e ; i++){
        sf::Color col;
        if(dark_mode_on==true){
            col = sf::Color(240,240,240);
        }else{
            col = sf::Color(0,0,0);
        }
        //getting starting and ending points        
        float a_x = morphToScaleX(graph->positions[graph->elist[i].n1].getX(),graph) + origin_x + STD_NODE_SIZE_PX;
        float a_y = morphToScaleY(graph->positions[graph->elist[i].n1].getY() ,graph) + origin_y + STD_NODE_SIZE_PX;
        float b_x = morphToScaleX(graph->positions[graph->elist[i].n2].getX() ,graph) + origin_x + STD_NODE_SIZE_PX;
        float b_y = morphToScaleY(graph->positions[graph->elist[i].n2].getY() ,graph) + origin_y + STD_NODE_SIZE_PX;
        
        sf::RectangleShape tmp = createLine(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX,col);
        tmp.move(graph->delta_x, graph->delta_y);
        window->draw(tmp);
        if(graph->is_directed){            
            drawArrowHead(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX,col,window,graph);
        }
    }
    if(graph->pathProcessed){
        int nc = graph->target;
        int nl = -1;
        while(nl!=graph->start){
            unsigned int tmp = -1;
            // find vertices that are part of the path
            if(graph->is_directed){
                for(int i = graph->rcd[nc] ; i < graph->rcd[nc+1] ; i++){
                    if(graph->dist_to_origin[graph->radj[i]]<tmp){
                        tmp = graph->dist_to_origin[graph->radj[i]];
                        nl = graph->radj[i];
                    }
                }
            }else{
                for(int i = graph->cd[nc] ; i < graph->cd[nc+1] ; i++){
                    if(graph->dist_to_origin[graph->adj[i]]<tmp){
                        tmp = graph->dist_to_origin[graph->adj[i]];
                        nl = graph->adj[i];
                    }
                }  
            }                      
            //getting starting and ending points
            float a_x = morphToScaleX(graph->positions[nl].getX() , graph) + origin_x + STD_NODE_SIZE_PX;
            float a_y = morphToScaleY(graph->positions[nl].getY() , graph) + origin_y + STD_NODE_SIZE_PX;
            float b_x = morphToScaleX(graph->positions[nc].getX() , graph) + origin_x + STD_NODE_SIZE_PX;
            float b_y = morphToScaleY(graph->positions[nc].getY() , graph) + origin_y + STD_NODE_SIZE_PX;
            //
            sf::Color col;
            if(dark_mode_on==true){
                col = sf::Color(200,200,0);
            }else{
                col = sf::Color(255,255,0);
            }
            sf::RectangleShape art = createLine(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX*2,col);
            art.move(graph->delta_x, graph->delta_y);
            window->draw(art);
            if(graph->is_directed){            
                drawArrowHead(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX*2,col,window,graph);
            }
            
            nc = nl;
        }

    }
    //drawing nodes
    for(int i = 0 ; i < graph->n ; i ++){
        //create circleshape for node
        sf::CircleShape node(STD_NODE_SIZE_PX);
        if(dark_mode_on==true){
            node.setOutlineThickness(STD_NODE_SIZE_PX/10);
            node.setOutlineColor(sf::Color(240,240,240));
        }
        if(graph->node_type_list[i]==TARGET){
            node.setFillColor(sf::Color(255, 0, 0));
        }else if ( graph->node_type_list[i]==START) {
            node.setFillColor(sf::Color(0, 255, 0));
        }else{
            node.setFillColor(sf::Color(0, 0, 255));
        }
        
        node.setPosition(morphToScaleX(graph->positions[i].getX(), graph)+origin_x,morphToScaleY(graph->positions[i].getY(),graph)+origin_y);
        node.move(graph->delta_x   , graph->delta_y );
        //drawing node
        window->draw(node);
    }
}

sf::RectangleShape createLine(float x1,float y1, float x2, float y2, float width, sf::Color color){
    float dist = distance(Point(x1,y1),Point(x2,y2));
    sf::RectangleShape tmp(sf::Vector2f(dist, width));
    tmp.setFillColor(color);
    tmp.setPosition(x1,y1);
    float angle = acos(abs(x1-x2)/dist)*180/M_PI;
    if(x1<x2){
        if(y1<y2){
            tmp.rotate(angle);
        }else{
            tmp.rotate(-angle);
        }
    }else{
        if(y1<y2){
            tmp.rotate(180-angle);
        }else{
            tmp.rotate(180+angle);
        }
    }
    //tmp.move(sf::Vector2f(0, -width/2));
    Vector orthogonal(y2-y1,-(x2-x1));
    orthogonal = orthogonal/orthogonal.getNorm()*(width/2);
    tmp.move(sf::Vector2f(orthogonal.getVX(),orthogonal.getVY()));
    return tmp;
}
void drawArrowHead(float a_x,float a_y, float b_x, float b_y, float width, sf::Color color,sf::RenderWindow * window, AdjArray* graph){
    sf::RectangleShape arrow(sf::Vector2f(STD_NODE_SIZE_PX*0.75,width));
    arrow.setFillColor(color);
    arrow.move(sf::Vector2f(b_x+graph->delta_x,b_y+graph->delta_y));
    Vector translate(b_x-a_x,b_y-a_y);
    float dist = distance(Point(a_x,a_y),Point(b_x,b_y));
    translate = translate/translate.getNorm();
    arrow.move(sf::Vector2f(-translate.getVX()*STD_NODE_SIZE_PX,-translate.getVY()*STD_NODE_SIZE_PX));
    Vector orthogonal(b_y-a_y,-(b_x-a_x));
    orthogonal = orthogonal/orthogonal.getNorm();
    arrow.move(sf::Vector2f(-orthogonal.getVX()*width/2,-orthogonal.getVY()*width/2));            
    float angle = acos(abs(a_x-b_x)/dist)*180/M_PI;
    if(a_x<b_x){
        if(a_y<b_y){
            arrow.rotate(angle);
        }else{
            arrow.rotate(-angle);
        }
    }else{
        if(a_y<b_y){
            arrow.rotate(180-angle);
        }else{
            arrow.rotate(180+angle);
        }
    }
    arrow.rotate(180-20);
    window->draw(arrow);
    arrow.rotate(2*20);
    window->draw(arrow);
}
void drawGraphCommunities(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph){
    //drawing lines
    for(int i = 0; i < graph->e ; i++){
        sf::Color col;
        if(dark_mode_on==true){
            col = sf::Color(240,240,240);
        }else{
            col = sf::Color(0,0,0);
        }
        //getting starting and ending points        
        float a_x = morphToScaleX(graph->positions[graph->elist[i].n1].getX(),graph) + origin_x + STD_NODE_SIZE_PX;
        float a_y = morphToScaleY(graph->positions[graph->elist[i].n1].getY() ,graph) + origin_y + STD_NODE_SIZE_PX;
        float b_x = morphToScaleX(graph->positions[graph->elist[i].n2].getX() ,graph) + origin_x + STD_NODE_SIZE_PX;
        float b_y = morphToScaleY(graph->positions[graph->elist[i].n2].getY() ,graph) + origin_y + STD_NODE_SIZE_PX;
        
        sf::RectangleShape tmp = createLine(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX,col);
        tmp.move(graph->delta_x, graph->delta_y);
        window->draw(tmp);
        if(graph->is_directed){            
            drawArrowHead(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX,col,window,graph);
        }
    }
    //drawing nodes
    for(int i = 0 ; i < graph->n ; i ++){
        //create circleshape for node
        sf::CircleShape node(STD_NODE_SIZE_PX);  
        if(dark_mode_on==true){
            node.setOutlineThickness(STD_NODE_SIZE_PX/10);
            node.setOutlineColor(sf::Color(240,240,240));
        }
        if(graph->communities_processed==true){
            node.setFillColor(graph->labels_colors[graph->labels[i]]);
        }else{
            node.setFillColor(sf::Color(0, 0, 255));  
        }
        node.setPosition(morphToScaleX(graph->positions[i].getX(), graph)+origin_x,morphToScaleY(graph->positions[i].getY(),graph)+origin_y);
        node.move(graph->delta_x, graph->delta_y);
        //drawing node
        window->draw(node);
    }
}
void drawGraphPageRank(sf::RenderWindow * window, float origin_x, float origin_y, AdjArray * graph){
    for(int i = 0; i < graph->e ; i++){
        sf::Color col;
        if(dark_mode_on==true){
            col = sf::Color(240,240,240);
        }else{
            col = sf::Color(0,0,0);
        }
        //getting starting and ending points        
        float a_x = morphToScaleX(graph->positions[graph->elist[i].n1].getX(),graph) + origin_x + STD_NODE_SIZE_PX;
        float a_y = morphToScaleY(graph->positions[graph->elist[i].n1].getY() ,graph) + origin_y + STD_NODE_SIZE_PX;
        float b_x = morphToScaleX(graph->positions[graph->elist[i].n2].getX() ,graph) + origin_x + STD_NODE_SIZE_PX;
        float b_y = morphToScaleY(graph->positions[graph->elist[i].n2].getY() ,graph) + origin_y + STD_NODE_SIZE_PX;
        
        sf::RectangleShape tmp = createLine(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX,col);
        tmp.move(graph->delta_x, graph->delta_y);
        window->draw(tmp);
        if(graph->is_directed){            
            drawArrowHead(a_x,a_y,b_x,b_y,STD_VERTEX_SIZE_PX,col,window,graph);
        }
    }
    //drawing nodes
    for(int i = 0 ; i < graph->n ; i ++){
        //create circleshape for node
        sf::CircleShape node(STD_NODE_SIZE_PX);  
        if(dark_mode_on==true){
            node.setOutlineThickness(STD_NODE_SIZE_PX/10);
            node.setOutlineColor(sf::Color(240,240,240));
        }
        if(graph->pagerank_processed==true && i==graph->highest_pagerank){
            node.setFillColor(sf::Color(255, 0, 255));
        }else{
            node.setFillColor(sf::Color(0, 0, 255));  
        }
        node.setPosition(morphToScaleX(graph->positions[i].getX(), graph)+origin_x,morphToScaleY(graph->positions[i].getY(),graph)+origin_y);
        node.move(graph->delta_x, graph->delta_y);
        //drawing node
        window->draw(node);
    }
}


void printPositions(AdjArray * graph){
    for(int i =0; i<graph->n ; i++){
        cout << "node " << i << " is at position x = " << graph->positions[i].getX() << " , y = " << graph->positions[i].getY() << endl;
    }
}

float morphToScaleX(float f ,AdjArray* graph){
    return (graph->o_width/2 - graph->scale*graph->o_width/2) + f*graph->scale;
}
float morphToScaleY(float f ,AdjArray* graph){
    return (graph->o_height/2 - graph->scale*graph->o_height/2) + f*graph->scale;
}

