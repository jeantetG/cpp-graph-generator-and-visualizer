// ------------------------------------------------
// pagerank.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/pagerank.h"

void processPageRank(AdjArray* graph){
    // will be used as vector I see later
    float base = 1.0/graph->n;
    //initialising pagerank vector
    float* pagerank_vector = (float*) malloc(graph->n*sizeof(float));
    if(pagerank_vector == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    for(int i = 0; i < graph->n; i++){
        pagerank_vector[i] = base;
    }
    //initialising transition matrix
    //This takes a lot of memory for big graphs
    //but fow now we are working on small graphs so it's okay
    //for big graphs we can calculate it on the fly
    float** transition_matrix = (float**) malloc(graph->n*sizeof(float*));
    if(transition_matrix == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    for(int i = 0 ; i < graph->n ; i ++){
        transition_matrix[i] = (float*) malloc(graph->n*sizeof(float));
        if(transition_matrix[i] == NULL){
            cerr << "couldn't allocate memory, aborting" << endl;
            exit(1);
        }
    }
    for(unsigned long v = 0; v < graph->n ; v++){
        for(unsigned long u = 0; u < graph->n ; u++){
            /*
            if(u==v){
                continue;
            }*/
            //figuring out if u is a father of v
            bool is_father = false;
            for(unsigned long i=graph->cd[u]; i < graph->cd[u+1] ;i++){
                if(graph->adj[i] == v){
                    is_father = true;
                    break;
                }
            }
            if(is_father==true){
                // probability of father passing pagerank is 1/(degree of father)
                transition_matrix[v][u] = 1.0/(graph->cd[u+1]-graph->cd[u]);
            }else if (graph->cd[u+1]-graph->cd[u] > 0){
                // if not my father and has sons, no chance of passing pagerank to me
                transition_matrix[v][u] = 0;
            }else{
                //not my father and has no sons, giving a uniform probability to all others to avoid sinks
                transition_matrix[v][u] = base;
            }
        }
    }
    
    // now iterating to calculate the pagerank
    float* tmp_pagerank_vector = (float*) malloc(graph->n*sizeof(float));
    if(tmp_pagerank_vector == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    for(int i = 0; i < MAX_ITER ; i ++){
        // reinitialising the tmp pagerank vector
        for(int j = 0; j < graph->n; j++){
            tmp_pagerank_vector[j] = 0;
        }
        
        // This is corresponds to : (1-beta)*T*P+beta*I -> I
        for(int j = 0; j < graph->n ; j++){
            // T*P
            for(int k = 0; k < graph->n ; k++){
                tmp_pagerank_vector[j] += pagerank_vector[k]*transition_matrix[j][k];
            }
            // (1-beta)*P'+beta*I
            tmp_pagerank_vector[j] = (1-BETA)*tmp_pagerank_vector[j] + BETA*base;
        }        
        // now normalizing
        // getting the norm1
        float norm_1 = 0;   
        for(int j = 0; j < graph->n; j++){
            norm_1 += tmp_pagerank_vector[j];
        }      
        for(int j = 0; j < graph->n; j++){
            // normalization
            tmp_pagerank_vector[j] += (1-norm_1)*base;
        }
        //before updating checking if should
        //stop because of convergence
        int stop=0;
        if(i>1){
            for(int j = 0 ; j < graph->n ; j++){
                if(abs(pagerank_vector[j] - tmp_pagerank_vector[j])<PRECISION){
                    stop++;
                }
            }
        }
               
        //update
        for(int j = 0; j < graph->n; j++){
            //updating
            pagerank_vector[j] = tmp_pagerank_vector[j];
        }     
        if(stop==graph->n){
            break;
        }   
    }
    // now finding the node with highest score
    unsigned long elected = 0;
    float pr = pagerank_vector[0];
    for(unsigned long i = 1; i < graph->n ; i++){
        if(pagerank_vector[i]>pr){
            elected = i;
            pr = pagerank_vector[i];
        }
    }   
    graph->pagerank_processed=true;
    graph->highest_pagerank=elected;

    //freeing
    free(pagerank_vector);
    free(tmp_pagerank_vector);
    for(int i = 0 ; i < graph->n ; i ++){
        free(transition_matrix[i]);
    }
    free(transition_matrix);
}