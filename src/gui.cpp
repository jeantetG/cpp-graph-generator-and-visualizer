// ------------------------------------------------
// gui.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/gui.h"



//************************* Button *************************//
Button::Button(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string textarg, int textSize, void (*cb)(void*)){

    renderWindow = rw;
    //Creating the box and translating it    
    try
    {
        box = new sf::RectangleShape(sf::Vector2f(w, h));
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
box->setFillColor(BLUE);
    box->setPosition(px,py);
    box->setOutlineThickness(1);

    //Loading the font    
    try
    {
        font = new sf::Font();
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    if (!font->loadFromFile(fontPath)){
        cerr << "Couldn't load font, exiting" << endl;
        exit(1);
    }

    //Creating the text and fitting it in the box    
    try
    {
        text = new sf::Text(textarg, *font,textSize);
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    float text_height = text->getLocalBounds().height;
    float text_width = text->getLocalBounds().width;
    float text_x = px + w/2 - text_width/2;
    float text_y = py + h/2 - text_height/2;
    text->setPosition(text_x,text_y);
    text->setFillColor(sf::Color(255, 255, 255));

    //Initialising the callback function
    callback=cb;
    

}
Button::~Button(){
    delete box;
    delete font;
    delete text;
}
//************************* Button Getters *************************//
sf::RectangleShape * Button::getBox(){
    return box;
}

//************************* Button Methods *************************//
void Button::drawButton(){
    //draw box
    renderWindow->draw(*box);
    //draw text
    renderWindow->draw(*text);

}
bool Button::clicked(float x, float y){

    bool x_test = (x <= (box->getPosition().x + box->getGlobalBounds().width)) && (x >= (box->getPosition().x));
    bool y_test = (y <= (box->getPosition().y + box->getGlobalBounds().height)) && (y >= (box->getPosition().y));

    return x_test && y_test;

}
void Button::ifClickedCallBack(void* b, float x, float y){
    if(clicked(x,y)){
        callback(b);
    }
}
//************************* CheckBox *************************//
CheckBox::CheckBox(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string textarg, int textSize, bool c ){
    renderWindow = rw;
    //Creating the box and translating it
    try
    {
        box = new sf::RectangleShape(sf::Vector2f(w, h));
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    box->setFillColor(BLUE);
    box->setPosition(px,py);
    box->setOutlineThickness(1);

    //Loading the font
    font = new sf::Font();
    try
    {
        font = new sf::Font();
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    if (!font->loadFromFile(fontPath)){
        cerr << "Couldn't load font, exiting" << endl;
        exit(1);
    }

    //Creating the text and fitting it in the box
    text = new sf::Text(textarg, *font,textSize);
    try
    {
        text = new sf::Text(textarg, *font,textSize);
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    float text_height = text->getLocalBounds().height;
    float text_width = text->getLocalBounds().width;
    float text_x = px + w/2 - text_width;
    float text_y = py + box->getGlobalBounds().height/2 - text_height/2;
    text->setPosition(text_x,text_y);
    text->setFillColor(sf::Color(255, 255, 255));

    checked=c;
}
CheckBox::~CheckBox(){
    delete box;
    delete font;
    delete text;
}

void CheckBox::drawCheckBox(){
    //draw box
    renderWindow->draw(*box);
    //draw text
    renderWindow->draw(*text);
    //draw checkbox
    sf::RectangleShape tmp(sf::Vector2f(box->getGlobalBounds().height/2, box->getGlobalBounds().height/2));
    if(checked){
        tmp.setFillColor(sf::Color(0, 255, 0));
    }else{
        tmp.setFillColor(sf::Color(255, 0, 0));
    }    
    tmp.setPosition(box->getGlobalBounds().width-box->getGlobalBounds().height/4*3 + box->getPosition().x,box->getGlobalBounds().height/4 + box->getPosition().y);    
    renderWindow->draw(tmp);
}
bool CheckBox::clicked(float x, float y){
    bool x_test = (x <= (box->getPosition().x + box->getGlobalBounds().width)) && (x >= (box->getPosition().x));
    bool y_test = (y <= (box->getPosition().y + box->getGlobalBounds().height)) && (y >= (box->getPosition().y));

    return x_test && y_test;
}
void CheckBox::flip(){
    checked = !checked;
}
bool CheckBox::getPayload(){
    return checked;
}
//************************* TextBox *************************//
TextBox::TextBox(float w, float h, float px, float py, sf::RenderWindow * rw, string fontPath, string textarg, int textSize, string default_prompt){

    renderWindow = rw;
    is_selected = false;
    _w=w;
    _h=h;
    _px=px;
    _py=py;
    //Creating the box and translating it
    try
    {
        box = new sf::RectangleShape(sf::Vector2f(w, h));
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    box->setFillColor(BLUE);
    box->setPosition(px,py);
    box->setOutlineThickness(1);

    //Loading the font
    font = new sf::Font();
    try
    {
        font = new sf::Font();
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    if (!font->loadFromFile(fontPath)){
        cerr << "Couldn't load font, exiting" << endl;
        exit(1);
    }
    //Creating the text and fitting it in the box
    text = new sf::Text(textarg, *font,textSize);
    try
    {
        text = new sf::Text(textarg, *font,textSize);
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    float text_width = text->getLocalBounds().width;
    float text_x = px + w/2 - text_width/2;
    float text_y = py ;
    text->setPosition(text_x,text_y);
    text->setFillColor(sf::Color(255, 255, 255));  
    //Creating the textbox and translating it    
    try
    {
        textbox = new sf::RectangleShape(sf::Vector2f(w-w/10, h/2));
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    textbox->setFillColor(sf::Color(255, 255, 255));
    textbox->setPosition(px+w/10/2,py+(h/2)-(h/8));
    textbox->setOutlineColor(sf::Color(255, 255, 255));
    textbox->setOutlineThickness(4);
    //Creating the textprompt and fitting it in the textbox    
    try
    {
        textPrompt = new sf::Text(default_prompt, *font,textSize);
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    float text_prompt_height = textPrompt->getLocalBounds().height;
    float text_prompt_width = textPrompt->getLocalBounds().width;
    float text_prompt_x = px+w/10/2 + (w-w/10)/2 - text_prompt_width/2;
    float text_prompt_y = py+(h/2)-(h/8) + h/2/2 - text_prompt_height/2;
    textPrompt->setPosition(text_prompt_x,text_prompt_y);
    textPrompt->setFillColor(sf::Color(0, 0, 0)); 
    //Creating the textpromptmp and fitting it
    try
    {
        textPromptTmp = new sf::Text("", *font,textSize);
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }    
    textPromptTmp->setPosition(text_prompt_x + text_prompt_width/2,text_prompt_y);
    textPromptTmp->setFillColor(sf::Color(255, 0, 255)); 
}
TextBox::~TextBox(){
    delete box;
    delete font;
    delete text;
    delete textbox;
    delete textPrompt;
    delete textPromptTmp;
}
//************************* Setters TextBox *************************//
void TextBox::setTextPrompt(string t){
    textPrompt->setString(t);
}
void TextBox::setTextPromptTmp(string t){
    textPromptTmp->setString(t);
}

//************************* Methods TextBox *************************//
void TextBox::drawTextBox(){
    renderWindow->draw(*box);
    renderWindow->draw(*text);
    renderWindow->draw(*textbox);
    if(is_selected){
        textbox->setOutlineColor(sf::Color(255,0,255));
        auto s = textPromptTmp->getString().getSize();
        if(s==0){
            renderWindow->draw(*textPrompt);
        }else{
            renderWindow->draw(*textPromptTmp);
        }
    }else{
        textbox->setOutlineColor(sf::Color(255,255,255));
        renderWindow->draw(*textPrompt);
    }        
}

bool TextBox::clicked(float x, float y){
    bool x_test = (x <= (box->getPosition().x + box->getGlobalBounds().width)) && (x >= (box->getPosition().x));
    bool y_test = (y <= (box->getPosition().y + box->getGlobalBounds().height)) && (y >= (box->getPosition().y));
    bool res=x_test && y_test;

    if(res)
        is_selected = true;

    return res;
}
void TextBox::setSelected(bool b){
    is_selected = b;
}
sf::Text* TextBox::getTextPromptTmp(){
    return textPromptTmp;
}
sf::Text* TextBox::getTextPrompt(){
    return textPrompt;
}
float TextBox::getW(){
    return _w;
}
float TextBox::getH(){
    return _h;
}
float TextBox::getPX(){
    return _px;
}
float TextBox::getPY(){
    return _py;
}
//************************* TextBoxInt *************************//
void TextBoxInt::setPayload(int i){
    payload=i;
}
int TextBoxInt::getPayload(){
    return payload;
}

//************************* TextBoxFloat *************************//
void TextBoxFloat::setPayload(float i){
    payload=i;
}
float TextBoxFloat::getPayload(){
    return payload;
}
//************************* TextBoxString *************************//
void TextBoxString::setPayload(string i){
    payload = i;
}
string TextBoxString::getPayload(){
    return payload;
}
//************************* Tool Callback functions *************************//
void switch_to_pathfinding(void * b){
    *((ActiveSceneType *)b)=PATH_FINDING;
}

void switch_to_communities(void * b){
    *((ActiveSceneType *)b)=COMMUNITIES;
}

void switch_to_pagerank(void * b){
    *((ActiveSceneType *)b)=PAGERANK;
}

void startBFS(void *b){
    WGarg* wgarg = (WGarg* )b;
    if(wgarg->graph->target==-1 || wgarg->graph->start==-1){
        return;
    }
    shortestPathBFS(wgarg);
}

void resetVIEW(void* b){
    AdjArray* graph = (AdjArray*)b;
    graph->pathProcessed = false;
}

void loadGeneratedGraph(void *b){
    AdjArray* graph = (AdjArray*)b;
    if(graph->is_directed){
        graph->loadAdjArrayDirected("./tests/generated_graph");
    }else{
        graph->loadAdjArrayUndirected("./tests/generated_graph");
    }
}
