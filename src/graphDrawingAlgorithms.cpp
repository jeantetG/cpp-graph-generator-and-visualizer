// ------------------------------------------------
// graphDrawingAlgorithms.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/graphDrawingAlgorithms.h"
#include <limits>


WGarg::WGarg(vector<sf::CircleShape>* an,vector<VertexArgs>* av,AdjArray* g){
    animations_node=an;
    animations_vertex=av;
    graph=g;
}



void springDrawGenerate(float width, float height, AdjArray * graph, int max_iteration){

    graph->o_width = width;
    graph->o_height = height;    

    //initializing starting positions at random
    for(int i = 0 ; i < graph->n ; i++){
        graph->positions[i].setX(rand() % (int)width);
        graph->positions[i].setY(rand() % (int)height);
    }
    
    //creating array of force vector
    Vector* forces = (Vector*)calloc(graph->n,sizeof(Vector));
    if(forces == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }

    //Creating useful variables
    float force;
    Point* a;
    Point* b;
    Vector director_a_to_b;
    Vector director_b_to_a;
    
    //starting iterations and translations
    for(int i =0; i < max_iteration ; i++){
        //processing attraction force
        for(int j=0; j < graph->e ; j++){
            //get the force
            a = &graph->positions[graph->elist[j].n1];
            b = &graph->positions[graph->elist[j].n2];
            force = C1*log(distance(*a,*b)/C2);
            //get the vectors for direction
            director_a_to_b = vectorDirector(*a,*b);
            director_b_to_a = vectorDirector(*b,*a);
            //add to the forces array
            forces[graph->elist[j].n1].translate(director_a_to_b*force/director_a_to_b.getNorm());
            forces[graph->elist[j].n2].translate(director_b_to_a*force/director_b_to_a.getNorm());
        }
        //processing repel force
        for(int j=0; j < graph->n ; j++){
            for(int k=0; k < graph->n ; k++){
                if(j!=k && !graph->areNeighboursUndirected(j,k)){
                    //get the force
                    a = &graph->positions[j];
                    b = &graph->positions[k];
                    force = C3/sqrt(distance(*a,*b));
                    //get the vectors for direction
                    director_a_to_b = vectorDirector(*a,*b);
                    director_b_to_a = vectorDirector(*b,*a);
                    //add to the forces array
                    forces[j].translate(director_b_to_a*force/director_b_to_a.getNorm());
                    forces[k].translate(director_a_to_b*force/director_a_to_b.getNorm());
                }
            }
        }
        //applying the forces
        for(int j=0; j < graph->n ; j++){
            graph->positions[j].translate(forces[j]);
            //resetting the forces
            forces[j].setVX(0.0f);
            forces[j].setVY(0.0f);
        }        

    }
    //scaling
    //getting extremas
    float min_x = numeric_limits<float>::max();
    float max_x = 0.0f;
    float min_y = numeric_limits<float>::max();
    float max_y = 0.0f;
    for(int i = 0; i<graph->n ; i++){
        Point* p = &(graph->positions[i]);
        if(p->getX()<min_x)
            min_x=p->getX();
        if(p->getX()>max_x)
            max_x=p->getX();
        if(p->getY()<min_y)
            min_y=p->getY();
        if(p->getY()>max_y)
            max_y=p->getY();
    }
    //setting up scaling
    width -= STD_NODE_SIZE_PX*2;
    height -= STD_NODE_SIZE_PX*2;
    float non_normalized_width = max_x-min_x;
    float non_normalized_height = max_y-min_y;
    //normalizing and scaling
    for(int i = 0; i<graph->n ; i++){
        graph->positions[i].setX((graph->positions[i].getX() - min_x)/non_normalized_width*width);
        graph->positions[i].setY((graph->positions[i].getY() - min_y)/non_normalized_height*height);
    }
    // scaling node size and vertex size for readability
    
    if( graph->n > MAX_N_NODES_WITHOUT_SCALING ){
        auto tmp = BASE_STD_NODE_SIZE_PX*MAX_N_NODES_WITHOUT_SCALING/graph->n;
        STD_NODE_SIZE_PX = tmp > MIN_NODE_SIZE ? tmp : MIN_NODE_SIZE;
    }else{
        STD_NODE_SIZE_PX = BASE_STD_NODE_SIZE_PX;
    }
    float vertices_surface = 0;
    for(int i = 0; i < graph->e ; i++){
        vertices_surface += (distance(graph->positions[graph->elist[i].n1],graph->positions[graph->elist[i].n2])-2*STD_NODE_SIZE_PX)*STD_VERTEX_SIZE_PX;
    }
    if(vertices_surface > MAX_SURFACE_WITHOUT_SCALING){
        auto tmp = BASE_STD_VERTEX_SIZE_PX*MAX_SURFACE_WITHOUT_SCALING/vertices_surface;
        STD_VERTEX_SIZE_PX = tmp > MIN_VERTEX_SIZE ? tmp : MIN_VERTEX_SIZE;
    }else{
        STD_VERTEX_SIZE_PX = BASE_STD_VERTEX_SIZE_PX;
    }
    
    // freeing
    free(forces);

}

void randomDrawGenerate(float width, float height, AdjArray * graph){
    //initializing starting positions at random
    srand (time(NULL));
    for(int i = 0 ; i < graph->n ; i++){
        graph->positions[i].setX(rand() % (int)width);
        graph->positions[i].setY(rand() % (int)height);
    }
}