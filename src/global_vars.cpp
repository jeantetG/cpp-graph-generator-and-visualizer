// ------------------------------------------------
// global_vars.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 11-03-2020
//
// ------------------------------------------------

#include "../include/global_vars.h"

//std window width
float STD_WINDOW_WIDTH = 1920.0f;
//gui size
float WINDOW_WIDTH = 1920.0f;
float WINDOW_HEIGHT = 1080.0f;
int NB_BUTTONS = 3;
float BUTTON_HEIGHT = 60.0f;
float BUTTON_WIDTH = WINDOW_WIDTH/5;
float CANVAS_WIDTH = BUTTON_WIDTH*3;
float CANVAS_HEIGHT = WINDOW_HEIGHT-BUTTON_HEIGHT;
float TEXT_SIZE = 20;
float SELECTION_OUTLINE_WIDTH = 3;
// fifo max size
long NLINKS = 1000;
// animation speed
int STEP = 200; // in milliseconds
// graph drawing variables
float STD_NODE_SIZE_PX = 25.0f;
float STD_VERTEX_SIZE_PX = 5.0f;
float BASE_STD_NODE_SIZE_PX = 25.0f;
float BASE_STD_VERTEX_SIZE_PX = 5.0f;
int MAX_N_NODES_WITHOUT_SCALING = 25;
float MAX_SURFACE_WITHOUT_SCALING = 300000;
float MIN_NODE_SIZE = 5.0f;
float MIN_VERTEX_SIZE = 1.0f;
float MAX_ZOOM = 50000;
// spring constants
float C1 = 2.0f;
float C2 = 1.0f;
float C3 = 1.0f;
float C4 = 0.1f;
// pagerank variables
int MAX_ITER = 30;
float BETA = 0.15;
float PRECISION = 0.000001;
// gui colors
sf::Color BLUE = sf::Color(0, 102, 255);
sf::Color DARK_GREY = sf::Color(51,51,51);
sf::Color GREY = sf::Color(140, 140, 140);
bool dark_mode_on = true;