// ------------------------------------------------
// main.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/gui.h"
#include "../include/graphDrawingAlgorithms.h"
#include "../include/graphDrawing.h"
#include "../include/global_vars.h"
#include "../include/graph_generator.h"
#include "../include/communities.h"
#include "../include/pagerank.h"
#include <chrono>
#include <unistd.h>

#define MOUSE_POS sf::Mouse::getPosition(window).x,sf::Mouse::getPosition(window).y

int main(int argc, char** argv)
{
    
    if(argc != 4){
        cerr << "Please use make run or run-d to run this program" << endl;
        exit(1);
    }
    // Handling dark mode
    dark_mode_on = atoi(argv[3]);
    if(dark_mode_on == true){
        GREY = sf::Color(0,0,0);
        BLUE = sf::Color(0,0,0);
        DARK_GREY = sf::Color(0,0,0);
    }
    //window scaling 
    int new_width = atoi(argv[1]) ;
    int new_height = atoi(argv[2]);
    if(new_width!=STD_WINDOW_WIDTH){
        if(((float)new_width/new_height - 16.0/9.0) > 0.0000001){
            cerr << "Please choose a 16/9 display resolution." << endl;
            exit(1);
        }else{
            float new_resolution_scale = new_width / STD_WINDOW_WIDTH;
            WINDOW_WIDTH*=new_resolution_scale;
            WINDOW_HEIGHT*=new_resolution_scale;            
            BUTTON_HEIGHT*=new_resolution_scale;
            BUTTON_WIDTH*=new_resolution_scale;
            CANVAS_WIDTH*=new_resolution_scale;
            CANVAS_HEIGHT*=new_resolution_scale;           
            STD_NODE_SIZE_PX*=new_resolution_scale;
            STD_VERTEX_SIZE_PX*=new_resolution_scale;
            BASE_STD_NODE_SIZE_PX*=new_resolution_scale;
            BASE_STD_VERTEX_SIZE_PX*=new_resolution_scale;
            MAX_SURFACE_WITHOUT_SCALING*=new_resolution_scale;
            MIN_NODE_SIZE*=new_resolution_scale;
            MIN_VERTEX_SIZE*=new_resolution_scale;
            TEXT_SIZE*=new_resolution_scale;
            SELECTION_OUTLINE_WIDTH*=new_resolution_scale;
        }
    }
    //************************* Setting rand seed *************************//
    srand (time(0));
    //************************* Setting up GUI *************************//
    sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Graphs Visualizer", sf::Style::Titlebar | sf::Style::Close);

	window.setFramerateLimit(120);

    Button* button_list[NB_BUTTONS];

    ActiveSceneType active_scene = PATH_FINDING;
    //************************* Buttons *************************//
    // Top Buttons
    button_list[0] = new Button(BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_WIDTH, 0, &window, "./media/Oxanium-Medium.ttf", "PathFinding", TEXT_SIZE, switch_to_pathfinding);
    button_list[1] = new Button(BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_WIDTH*2, 0, &window, "./media/Oxanium-Medium.ttf", "Communities", TEXT_SIZE, switch_to_communities);
    button_list[2] = new Button(BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_WIDTH*3, 0, &window, "./media/Oxanium-Medium.ttf", "PageRank", TEXT_SIZE, switch_to_pagerank);
    // Reset view Button
    Button resetView(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT-BUTTON_HEIGHT*1.5f-1, &window, "./media/Oxanium-Medium.ttf", "ResetView", TEXT_SIZE, resetVIEW);
    //************************* Lines *************************//
    sf::Vertex line_left[] =
    {
        sf::Vertex(sf::Vector2f(BUTTON_WIDTH, 0)),
        sf::Vertex(sf::Vector2f(BUTTON_WIDTH, WINDOW_HEIGHT))
    };
    sf::Vertex line_right[] =
    {
        sf::Vertex(sf::Vector2f(BUTTON_WIDTH*4+1, 0)),
        sf::Vertex(sf::Vector2f(BUTTON_WIDTH*4+1, WINDOW_HEIGHT))
    };
    //************************* panels *************************//
    sf::RectangleShape leftPanel(sf::Vector2f(BUTTON_WIDTH,WINDOW_HEIGHT));
    sf::RectangleShape rightPanel(sf::Vector2f(BUTTON_WIDTH,WINDOW_HEIGHT));
    rightPanel.setPosition(BUTTON_WIDTH*4+1,0);
    leftPanel.setFillColor(DARK_GREY);
    rightPanel.setFillColor(DARK_GREY);
    //************************* texts *************************//
    //************************* Canvas *************************//
    sf::RectangleShape canvas(sf::Vector2f(CANVAS_WIDTH, CANVAS_HEIGHT));
    canvas.setPosition(BUTTON_WIDTH,BUTTON_HEIGHT);
    canvas.setFillColor(GREY);
    //************************* Path Finding Scene *************************//
    Button startBFSPathFinding(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, BUTTON_WIDTH*4+1, WINDOW_HEIGHT/2, &window, "./media/Oxanium-Medium.ttf", "Run BFS", TEXT_SIZE, startBFS);
    //************************* Default graph *************************//
    AdjArray* aj;
    try
    {
        aj= new AdjArray();
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    bool graph_generated = true;
    aj->loadAdjArrayDirected("./tests/tmp3");
    springDrawGenerate(CANVAS_WIDTH,CANVAS_HEIGHT,aj,100);
    //************************* Communities Scene *************************//
    Button startCommunities(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, BUTTON_WIDTH*4+1, WINDOW_HEIGHT/2, &window, "./media/Oxanium-Medium.ttf", "Run Communities", TEXT_SIZE, nullptr);
    //************************* Communities Scene *************************//
    Button startPageRank(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, BUTTON_WIDTH*4+1, WINDOW_HEIGHT/2, &window, "./media/Oxanium-Medium.ttf", "Run PageRank", TEXT_SIZE, nullptr);
    //************************* Generator Scene *************************//
    TextBoxInt nodes_number(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT/2, &window, "./media/Oxanium-Medium.ttf", "How many nodes ?", TEXT_SIZE,2);
    TextBoxInt cluster_size(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT/2+BUTTON_HEIGHT*1.5f, &window, "./media/Oxanium-Medium.ttf", "Cluster size ?", TEXT_SIZE,1);
    TextBoxInt step_time(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, BUTTON_WIDTH*4+1, WINDOW_HEIGHT-BUTTON_HEIGHT*1.5f, &window, "./media/Oxanium-Medium.ttf", "Step time ? ( in ms )", TEXT_SIZE,STEP);
    TextBoxFloat cluster_connection(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT/2+BUTTON_HEIGHT*1.5f*2, &window, "./media/Oxanium-Medium.ttf", "Probability to cluster", TEXT_SIZE,0.5f);
    TextBoxFloat noncluster_connection(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT/2+BUTTON_HEIGHT*1.5f*3, &window, "./media/Oxanium-Medium.ttf", "Probability to non cluster", TEXT_SIZE,0.5f);
    Button generate(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT-2*BUTTON_HEIGHT*1.5f-1, &window, "./media/Oxanium-Medium.ttf", "Generate", TEXT_SIZE, loadGeneratedGraph);
    TextBoxString path_to_file(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT/2-BUTTON_HEIGHT*1.5f*3, &window, "./media/Oxanium-Medium.ttf", "File name in tests directory", TEXT_SIZE,"tmp3");
    Button generate_from_file(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT/2-BUTTON_HEIGHT*1.5f*2, &window, "./media/Oxanium-Medium.ttf", "Generate from file", TEXT_SIZE, nullptr);
    CheckBox is_directed(BUTTON_WIDTH, BUTTON_HEIGHT*1.5f, -1, WINDOW_HEIGHT/2-BUTTON_HEIGHT*1.5f*5, &window, "./media/Oxanium-Medium.ttf", "Directed graph ?", TEXT_SIZE,true);
    //************************* Creating my WGarg and useful variables *************************//
    vector<sf::CircleShape>* node_anims;
    try
    {
        node_anims = new vector<sf::CircleShape>();
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    vector<VertexArgs>* vertex_anims;
    try
    {
        vertex_anims = new vector<VertexArgs>();
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    WGarg* wgarg;
    try
    {
        wgarg = new WGarg(node_anims,vertex_anims,aj);
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
    }
    bool is_animating=false;
    int animation_state=0;
    auto last_anim_time = chrono::high_resolution_clock::now();
    auto current_time = chrono::high_resolution_clock::now();
    string usr_input = "";
    // Active Text Box
    TextBox* active_text_box=nullptr;
    // Handling mousewheel
    bool wheel_pressed;
    float wheel_x;
    float wheel_y;
    // Handling zone selection
    bool selecting = false;
    bool control_key_being_pressed = false;
    float selection_x;
    float selection_y;
    float ratio = CANVAS_WIDTH/CANVAS_HEIGHT;
    // Vectors
    vector<TextBoxInt*> text_boxs_int;
    text_boxs_int.push_back(&nodes_number);
    text_boxs_int.push_back(&cluster_size);
    text_boxs_int.push_back(&step_time);
    vector<TextBoxFloat*> text_boxs_float;
    text_boxs_float.push_back(&cluster_connection);
    text_boxs_float.push_back(&noncluster_connection);
    vector<TextBoxString*> text_boxs_string;
    text_boxs_string.push_back(&path_to_file);
    //************************* Starting main loop *************************//
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
                // Window is closed
                case sf::Event::Closed:
                    window.close();
                    break;
                // MouseWheel scroll scaling
                case sf::Event::MouseWheelScrolled:
                    {
                        auto test_w_x = event.mouseWheelScroll.x <= BUTTON_WIDTH*4 && event.mouseWheelScroll.x >= BUTTON_WIDTH;
                        auto test_w_y = event.mouseWheelScroll.y <= CANVAS_HEIGHT+BUTTON_HEIGHT && event.mouseWheelScroll.y >= BUTTON_HEIGHT;
                        if( test_w_x && test_w_y){
                            if(aj->scale + event.mouseWheelScroll.delta*aj->tenth_scale > 0 && aj->scale + event.mouseWheelScroll.delta*aj->tenth_scale < MAX_ZOOM){
                                aj->scale += event.mouseWheelScroll.delta*aj->tenth_scale;
                            }
                        }
                    } 
                    break;
                // Mouse Wheel Click
                case sf::Event::MouseButtonPressed:
                                   
                    if(event.mouseButton.button == sf::Mouse::Middle){
                        auto testx = sf::Mouse::getPosition(window).x <= BUTTON_WIDTH*4 && sf::Mouse::getPosition(window).x >= BUTTON_WIDTH;
                        auto testy = sf::Mouse::getPosition(window).y <= CANVAS_HEIGHT+BUTTON_HEIGHT && sf::Mouse::getPosition(window).y >= BUTTON_HEIGHT;
                        if( testx && testy){
                            wheel_pressed = true;
                            wheel_x = sf::Mouse::getPosition(window).x;
                            wheel_y = sf::Mouse::getPosition(window).y;
                        }                        
                    }
                    if(event.mouseButton.button == sf::Mouse::Left && control_key_being_pressed==true){
                        auto testx = sf::Mouse::getPosition(window).x <= BUTTON_WIDTH*4 && sf::Mouse::getPosition(window).x >= BUTTON_WIDTH;
                        auto testy = sf::Mouse::getPosition(window).y <= CANVAS_HEIGHT+BUTTON_HEIGHT && sf::Mouse::getPosition(window).y >= BUTTON_HEIGHT;
                        if( testx && testy){
                            selecting = true;
                            selection_x = sf::Mouse::getPosition(window).x;
                            selection_y = sf::Mouse::getPosition(window).y;
                        }                        
                    }
                    break;
                // Mouse Click
                case sf::Event::MouseButtonReleased:
                    //making sure this is released inside the canvas 
                    if(event.mouseButton.button == sf::Mouse::Middle){
                        auto testx = sf::Mouse::getPosition(window).x <= BUTTON_WIDTH*4 && sf::Mouse::getPosition(window).x >= BUTTON_WIDTH;
                        auto testy = sf::Mouse::getPosition(window).y <= CANVAS_HEIGHT+BUTTON_HEIGHT && sf::Mouse::getPosition(window).y >= BUTTON_HEIGHT;
                        if( testx && testy){
                            wheel_pressed = false;
                        } 
                    }
                    if(event.mouseButton.button == sf::Mouse::Left && selecting==true){
                        selecting = false;
                        // getting current position
                        auto current_x = sf::Mouse::getPosition(window).x;
                        auto current_y = sf::Mouse::getPosition(window).y;
                        // getting absolute offset
                        auto offset_x = abs(current_x-selection_x);
                        auto offset_y = abs(current_y-selection_y);

                        if(offset_x!=0 && offset_y!=0){
                            // getting end coordinate for the selection box
                            float end_x;
                            float end_y;
                            if(offset_x/offset_y>=ratio){
                                end_x = offset_x;                    
                                end_y = end_x/ratio;
                                
                            }else{
                                end_y = offset_y;                    
                                end_x = end_y*ratio;
                            }
                            float pos_x;
                            float pos_y;
                            if(current_x >= selection_x && current_y>=selection_y){
                                pos_x = selection_x;
                                pos_y = selection_y;
                            }else if(current_x >= selection_x && current_y<=selection_y){
                                pos_x = selection_x;
                                pos_y = selection_y-end_y;
                            }else if(current_x <= selection_x && current_y<=selection_y){
                                pos_x = selection_x-end_x;
                                pos_y = selection_y-end_y;
                            }else{
                                pos_x = selection_x-end_x;
                                pos_y = selection_y;
                            }
                            //scaling
                            float scaling = CANVAS_WIDTH/end_x;
                            if(aj->scale*scaling < MAX_ZOOM ){
                                aj->scale*=scaling;
                                // changing origin to canvas origin
                                pos_x -= BUTTON_WIDTH + aj->delta_x;
                                pos_y -= BUTTON_HEIGHT + aj->delta_y;
                                //translating
                                aj->delta_x = (CANVAS_WIDTH/2*scaling-CANVAS_WIDTH/2)-pos_x*scaling;
                                aj->delta_y = (CANVAS_HEIGHT/2*scaling-CANVAS_HEIGHT/2)-pos_y*scaling;
                            }
                        }                      
                    }else if (event.mouseButton.button == sf::Mouse::Left && control_key_being_pressed==false)
                    {
                        // Top buttons clicked
                        //stopping animations if necessary
                        if( button_list[0]->clicked(MOUSE_POS)){
                            if(active_scene!=PATH_FINDING){
                                is_animating=false;
                                animation_state=0;
                            }
                            button_list[0]->callback((void *)&active_scene);
                        }
                        if( button_list[1]->clicked(MOUSE_POS)){
                            if(active_scene!=COMMUNITIES){
                                is_animating=false;
                                animation_state=0;
                            }
                            button_list[1]->callback((void *)&active_scene);
                        }
                        if( button_list[2]->clicked(MOUSE_POS)){
                            if(active_scene!=PAGERANK){
                                is_animating=false;
                                animation_state=0;
                            }
                            button_list[2]->callback((void *)&active_scene);
                        }
                        // Checking generator textprompts int
                        bool reset_text_boxs = true;
                        for(auto i: text_boxs_int){
                            if(i->clicked(MOUSE_POS)){
                                if(active_text_box!=nullptr){
                                    if(active_text_box != i){
                                        active_text_box->setSelected(false);
                                        active_text_box = nullptr;
                                        usr_input = "";
                                        active_text_box = i;
                                        active_text_box->setTextPromptTmp(usr_input); 
                                    }                                
                                }else{
                                    active_text_box = i;
                                    active_text_box->setTextPromptTmp(usr_input);
                                    active_text_box->setSelected(true); 
                                }
                                reset_text_boxs = false;
                                break;                            
                            }
                        }
                        // Checking generator textprompts float
                        if(reset_text_boxs){
                            for(auto i: text_boxs_float){
                                if(i->clicked(MOUSE_POS)){
                                    if(active_text_box!=nullptr){
                                        if(active_text_box != i){
                                            active_text_box->setSelected(false);
                                            active_text_box = nullptr;
                                            usr_input = "";
                                            active_text_box = i;
                                            active_text_box->setTextPromptTmp(usr_input); 
                                        }                                
                                    }else{
                                        active_text_box = i;
                                        active_text_box->setTextPromptTmp(usr_input);
                                        active_text_box->setSelected(true); 
                                    }
                                    reset_text_boxs = false;
                                    break;                            
                                }
                            }
                        }
                        if(reset_text_boxs){
                            for(auto i: text_boxs_string){
                                if(i->clicked(MOUSE_POS)){
                                    if(active_text_box!=nullptr){
                                        if(active_text_box != i){
                                            active_text_box->setSelected(false);
                                            active_text_box = nullptr;
                                            usr_input = "";
                                            active_text_box = i;
                                            active_text_box->setTextPromptTmp(usr_input); 
                                        }                                
                                    }else{
                                        active_text_box = i;
                                        active_text_box->setTextPromptTmp(usr_input);
                                        active_text_box->setSelected(true); 
                                    }
                                    reset_text_boxs = false;
                                    break;                            
                                }
                            }
                        }
                        // if clicked elsewhere unselecting activetextprompt
                        if(reset_text_boxs){
                            if(active_text_box!=nullptr){
                                active_text_box->setSelected(false);
                                active_text_box = nullptr;
                                usr_input = ""; 
                            }                                                       
                        }
                        // Checking reset button
                        if(resetView.clicked(MOUSE_POS)){
                            resetView.callback((void*)aj);
                            is_animating=false;
                            animation_state=0;
                            aj->delta_x=0;
                            aj->delta_y=0;
                            aj->scale = aj->o_scale;
                            aj->communities_processed = false;
                            aj->pagerank_processed = false;
                        }
                        // Directed checkbox
                        if(is_directed.clicked(MOUSE_POS)){
                            is_directed.flip();
                        }    
                        // Checking generate button
                        if(generate.clicked(MOUSE_POS)){
                            bool empty = true;
                            while(empty){
                                if(is_directed.getPayload()){
                                    generate_graph_directed(nodes_number.getPayload(), cluster_size.getPayload(), cluster_connection.getPayload(), noncluster_connection.getPayload());
                                }else{
                                    generate_graph_undirected(nodes_number.getPayload(), cluster_size.getPayload(), cluster_connection.getPayload(), noncluster_connection.getPayload());
                                }
                                FILE *file = fopen("./tests/generated_graph", "r");
                                if (file == NULL) {
                                    cerr << "Couldn't load file" << endl;
                                    exit(1);
                                }
                                unsigned long a;
                                unsigned long b;
                                if(fscanf(file,"%lu %lu",&a,&b)==2){
                                    empty=false;
                                }
                                fclose(file);
                            }
                            if(graph_generated==true){
                                delete aj;
                                aj = new AdjArray();
                                try
                                {
                                    aj= new AdjArray();
                                }
                                catch (std::bad_alloc& ba)
                                {
                                    std::cerr << "bad_alloc caught: " << ba.what() << '\n';
                                }
                            }                            
                            if(is_directed.getPayload()){
                                aj->is_directed=true;
                            }else{
                                aj->is_directed=false;
                            }
                            generate.callback((void*)aj);
                            springDrawGenerate(CANVAS_WIDTH,CANVAS_HEIGHT,aj,100);
                            graph_generated=true;
                            is_animating=false;
                            animation_state=0;
                            wgarg->graph=aj;
                        }
                        // Checking generate from file button
                        if(generate_from_file.clicked(MOUSE_POS)){ 
                            if (FILE *file = fopen(("./tests/" + path_to_file.getPayload()).c_str(), "r")){
                                if (file == NULL) {
                                    cerr << "Couldn't load file" << endl;
                                    exit(1);
                                }
                                unsigned long a;
                                unsigned long b;
                                if(fscanf(file,"%lu %lu",&a,&b)!=2){
                                    fclose(file);
                                    cerr << "Couldn't load empty of misformatted file, exiting" << endl;
                                    exit(1);
                                }
                                fclose(file);
                                if(graph_generated==true){
                                    delete aj;
                                    aj = new AdjArray();
                                    try
                                    {
                                        aj= new AdjArray();
                                    }
                                    catch (std::bad_alloc& ba)
                                    {
                                        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
                                    }
                                }
                                if(is_directed.getPayload()){
                                    aj->loadAdjArrayDirected(("./tests/" + path_to_file.getPayload()));
                                }else{
                                    aj->loadAdjArrayUndirected(("./tests/" + path_to_file.getPayload()));
                                }                                
                                springDrawGenerate(CANVAS_WIDTH,CANVAS_HEIGHT,aj,100);
                                graph_generated=true;
                                is_animating=false;
                                animation_state=0;
                                wgarg->graph=aj;
                            }
                            
                        }                           
                        // IF PATHFINDING
                        if(active_scene == PATH_FINDING && graph_generated==true){
                            //testing if left clicked on a node
                            if(leftNodeClick(MOUSE_POS,BUTTON_WIDTH,BUTTON_HEIGHT,aj)){
                                is_animating=false;
                                animation_state=0;
                            }
                            //testing if BFS button was clicked
                            if(startBFSPathFinding.clicked(MOUSE_POS)){
                                animation_state=0;
                                aj->pathProcessed = false;
                                drawGraphPathFinding(&window,BUTTON_WIDTH,BUTTON_HEIGHT,aj);
                                startBFSPathFinding.callback((void*)wgarg);
                                if(aj->pathProcessed){
                                    is_animating = true;
                                    last_anim_time = chrono::high_resolution_clock::now();
                                }                                 
                                aj->pathProcessed = false;                                
                            }                            
                        }else if(active_scene == COMMUNITIES && graph_generated==true){     // IF COMMUNITIES                       
                            if(startCommunities.clicked(MOUSE_POS)){
                                processing_communities(aj);                                
                            }                            
                        }else if(active_scene == PAGERANK && graph_generated==true){  // IF PAGERANK                                          
                            if(startPageRank.clicked(MOUSE_POS)){
                                processPageRank(aj);                                
                            }                            
                        }                         
                    }
                    if (event.mouseButton.button == sf::Mouse::Right){
                        // IF PATHFINDING
                        if(active_scene == PATH_FINDING && graph_generated==true && control_key_being_pressed==false){
                            if(rightNodeClick(MOUSE_POS,BUTTON_WIDTH,BUTTON_HEIGHT,aj)){
                                is_animating=false;
                                animation_state=0;
                            }
                        }
                        // Unselecting generator textprompts if necessary
                        if(active_text_box!=nullptr){
                            if(!active_text_box->clicked(MOUSE_POS)){
                                active_text_box->setSelected(false);
                                active_text_box = nullptr;
                                usr_input = ""; 
                            }
                        }
                    }
                    break;              
                // User input
                case sf::Event::TextEntered:
                    //cout << "text en unicode " << event.text.unicode << endl;
                    //exit(1);
                    if(active_text_box!=nullptr){
                        if (event.text.unicode == '\b'){
                            if(usr_input.size()>0){
                                usr_input = usr_input.substr(0,usr_input.size()-1);
                                active_text_box->setTextPromptTmp(usr_input);
                                float text_prompt_height = active_text_box->getTextPromptTmp()->getLocalBounds().height;
                                float text_prompt_width = active_text_box->getTextPromptTmp()->getLocalBounds().width;
                                float text_prompt_x = active_text_box->getPX()+active_text_box->getW()/10/2 + (active_text_box->getW()-active_text_box->getW()/10)/2 - text_prompt_width/2;
                                float text_prompt_y = active_text_box->getPY()+(active_text_box->getH()/2)-(active_text_box->getH()/8) + active_text_box->getH()/2/2 - text_prompt_height/2;
                                active_text_box->getTextPromptTmp()->setPosition(text_prompt_x,text_prompt_y);
                            }
                        }
                        else if (event.text.unicode < 128 && event.text.unicode > 1 && event.text.unicode !=13){
                            usr_input += static_cast<char>(event.text.unicode);
                            active_text_box->setTextPromptTmp(usr_input);
                            float text_prompt_height = active_text_box->getTextPromptTmp()->getLocalBounds().height;
                            float text_prompt_width = active_text_box->getTextPromptTmp()->getLocalBounds().width;
                            float text_prompt_x = active_text_box->getPX()+active_text_box->getW()/10/2 + (active_text_box->getW()-active_text_box->getW()/10)/2 - text_prompt_width/2;
                            float text_prompt_y = active_text_box->getPY()+(active_text_box->getH()/2)-(active_text_box->getH()/8) + active_text_box->getH()/2/2 - text_prompt_height/2;
                            active_text_box->getTextPromptTmp()->setPosition(text_prompt_x,text_prompt_y);      
                        }                                          
                    }
                    break;             
                case sf::Event::KeyPressed:
                    //pressing control key
                    if( event.key.code==sf::Keyboard::LControl || event.key.code==sf::Keyboard::RControl){
                        control_key_being_pressed = true;
                    }        
                    break;
                case sf::Event::KeyReleased:
                    //releasing ctrl key
                    if( event.key.code==sf::Keyboard::LControl || event.key.code==sf::Keyboard::RControl){
                        control_key_being_pressed = false;
                    }
                    //Debug
                    if( event.key.code==sf::Keyboard::LAlt){
                        auto current_x = sf::Mouse::getPosition(window).x;
                        auto current_y = sf::Mouse::getPosition(window).y;
                        current_x -= BUTTON_WIDTH + aj->delta_x;
                        current_y -= BUTTON_HEIGHT + aj->delta_y;
                        cout << current_x << "    " << current_y << endl;
                    }
                    // Confirming input
                    //enter
                    if( event.key.code==sf::Keyboard::Return){
                        if(active_text_box!=nullptr){
                            bool go_text_boxs_int = false;
                            for(auto i: text_boxs_int){
                                if(active_text_box==i){
                                    go_text_boxs_int = true;
                                    break;
                                }
                            }
                            bool go_text_boxs_float = false;
                            for(auto i: text_boxs_float){
                                if(active_text_box==i){
                                    go_text_boxs_float = true;
                                    break;
                                }
                            }
                            bool go_text_boxs_string = false;
                            for(auto i: text_boxs_string){
                                if(active_text_box==i){
                                    go_text_boxs_string = true;
                                    break;
                                }
                            }
                            if(go_text_boxs_int){
                                int* i;
                                try{
                                    try
                                    {
                                        i = new int;
                                    }
                                    catch (std::bad_alloc& ba)
                                    {
                                        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
                                    }
                                    *i = stoi(usr_input);
                                    if((*i<=1 && active_text_box == &nodes_number) || (*i<=0 && active_text_box == &cluster_size)){
                                        delete i;
                                        i = nullptr;
                                    }
                                }catch(invalid_argument const &e){
                                    delete i;
                                    i = nullptr;
                                }
                                if(i!=nullptr){
                                    ((TextBoxInt *)active_text_box)->setPayload(*i);
                                    active_text_box->setTextPrompt(to_string(*i));
                                    usr_input = "";
                                    active_text_box->setTextPromptTmp(usr_input);
                                    active_text_box->setSelected(false);

                                    float text_prompt_height = active_text_box->getTextPrompt()->getLocalBounds().height;
                                    float text_prompt_width = active_text_box->getTextPrompt()->getLocalBounds().width;
                                    float text_prompt_x = active_text_box->getPX()+active_text_box->getW()/10/2 + (active_text_box->getW()-active_text_box->getW()/10)/2 - text_prompt_width/2;
                                    float text_prompt_y = active_text_box->getPY()+(active_text_box->getH()/2)-(active_text_box->getH()/8) + active_text_box->getH()/2/2 - text_prompt_height/2;
                                    active_text_box->getTextPrompt()->setPosition(text_prompt_x,text_prompt_y);

                                    active_text_box = nullptr;
                                    delete i;
                                    i=nullptr;
                                }else{
                                    usr_input = "";
                                    active_text_box->setTextPromptTmp(usr_input);
                                    active_text_box->setSelected(false);
                                    active_text_box = nullptr;
                                }
                            }
                            if(go_text_boxs_float){
                                double* i;
                                try{
                                    try
                                    {
                                        i = new double;
                                    }
                                    catch (std::bad_alloc& ba)
                                    {
                                        std::cerr << "bad_alloc caught: " << ba.what() << '\n';
                                    }
                                    *i = stod(usr_input);
                                    if(*i>1 || *i<0){
                                        delete i;
                                        i = nullptr;
                                    }
                                }catch(invalid_argument const &e){
                                    delete i;
                                    i = nullptr;
                                }
                                if(i!=nullptr){
                                    ((TextBoxFloat *)active_text_box)->setPayload(*i);
                                    active_text_box->setTextPrompt(to_string(*i));
                                    usr_input = "";
                                    active_text_box->setTextPromptTmp(usr_input);
                                    active_text_box->setSelected(false);

                                    float text_prompt_height = active_text_box->getTextPrompt()->getLocalBounds().height;
                                    float text_prompt_width = active_text_box->getTextPrompt()->getLocalBounds().width;
                                    float text_prompt_x = active_text_box->getPX()+active_text_box->getW()/10/2 + (active_text_box->getW()-active_text_box->getW()/10)/2 - text_prompt_width/2;
                                    float text_prompt_y = active_text_box->getPY()+(active_text_box->getH()/2)-(active_text_box->getH()/8) + active_text_box->getH()/2/2 - text_prompt_height/2;
                                    active_text_box->getTextPrompt()->setPosition(text_prompt_x,text_prompt_y);

                                    active_text_box = nullptr;
                                    delete i;
                                    i=nullptr;
                                }else{
                                    usr_input = "";
                                    active_text_box->setTextPromptTmp(usr_input);
                                    active_text_box->setSelected(false);
                                    active_text_box = nullptr;
                                }
                                
                            }
                            if(go_text_boxs_string){                               
                                
                                ((TextBoxString *)active_text_box)->setPayload(usr_input);
                                active_text_box->setTextPrompt(usr_input);
                                usr_input = "";
                                active_text_box->setTextPromptTmp(usr_input);
                                active_text_box->setSelected(false);

                                float text_prompt_height = active_text_box->getTextPrompt()->getLocalBounds().height;
                                float text_prompt_width = active_text_box->getTextPrompt()->getLocalBounds().width;
                                float text_prompt_x = active_text_box->getPX()+active_text_box->getW()/10/2 + (active_text_box->getW()-active_text_box->getW()/10)/2 - text_prompt_width/2;
                                float text_prompt_y = active_text_box->getPY()+(active_text_box->getH()/2)-(active_text_box->getH()/8) + active_text_box->getH()/2/2 - text_prompt_height/2;
                                active_text_box->getTextPrompt()->setPosition(text_prompt_x,text_prompt_y);

                                active_text_box = nullptr;                        
                                
                            }
                        }
                    }
                    break;
            }
                
        }
        // updating delta x and delta y
        if(wheel_pressed==true){
            //if out of canvas, unpressing the wheel
            auto testx = sf::Mouse::getPosition(window).x <= BUTTON_WIDTH*4 && sf::Mouse::getPosition(window).x >= BUTTON_WIDTH;
            auto testy = sf::Mouse::getPosition(window).y <= CANVAS_HEIGHT+BUTTON_HEIGHT && sf::Mouse::getPosition(window).y >= BUTTON_HEIGHT;
            if( !(testx && testy)){
                wheel_pressed = false;
            }
            // updating deltax
            auto tmp_x = sf::Mouse::getPosition(window).x;
            auto tmp_y = sf::Mouse::getPosition(window).y;
            aj->delta_x += tmp_x - wheel_x;
            aj->delta_y += tmp_y - wheel_y;
            wheel_x = tmp_x;
            wheel_y = tmp_y;            
        }             
        //************************* Drawing *************************//
        //Clearing
        window.clear();
        //Drawing canvas
        window.draw(canvas);
        //Drawing graph       
        if(active_scene == PATH_FINDING){
            if(graph_generated==true){
                drawGraphPathFinding(&window,BUTTON_WIDTH,BUTTON_HEIGHT,aj);
                // Drawing animations for pathfinding
                if(is_animating){
                    // drawing lines and nodes
                    for(int i = 0; i<animation_state; i++){
                    
                        sf::CircleShape node((*node_anims)[i]);
                        auto vertexToDraw = (*vertex_anims)[i];
                        //scaling
                        float nx1 = morphToScaleX( vertexToDraw.x1, aj ) + BUTTON_WIDTH+ STD_NODE_SIZE_PX ;
                        float nx2 = morphToScaleX( vertexToDraw.x2, aj ) + BUTTON_WIDTH+ STD_NODE_SIZE_PX ;
                        float ny1 = morphToScaleY( vertexToDraw.y1, aj ) + BUTTON_HEIGHT+ STD_NODE_SIZE_PX ;
                        float ny2 = morphToScaleY( vertexToDraw.y2, aj ) + BUTTON_HEIGHT+ STD_NODE_SIZE_PX ;

                        sf::Color col;
                        if(dark_mode_on==true){
                            node.setOutlineThickness(STD_NODE_SIZE_PX/10);
                            node.setOutlineColor(sf::Color(240,240,240));
                            col = sf::Color(200, 200, 0);
                        }else{
                            col = sf::Color(255, 255, 0);
                        } 
                        
                        sf::RectangleShape line = createLine(nx1,ny1,nx2,ny2,STD_VERTEX_SIZE_PX,col);
                        node.setPosition(sf::Vector2f(morphToScaleX( node.getPosition().x, aj ) + BUTTON_WIDTH ,  morphToScaleY( node.getPosition().y, aj ) + BUTTON_HEIGHT));
                        node.setRadius(node.getRadius());
                        //translating
                        line.move(aj->delta_x,aj->delta_y);
                        node.move(aj->delta_x,aj->delta_y);
                        window.draw(line);
                        window.draw(node);
                        if(aj->is_directed){
                            //auto arrowToDraw = (*arrow_anims)[i];
                            drawArrowHead(nx1,ny1,nx2,ny2,line.getSize().y,line.getFillColor(),&window,aj);
                        }
                    }
                    //cout << line_anims->size()<< " et " << animation_state <<endl;
                    // drawing start node
                    current_time = chrono::high_resolution_clock::now();
                    if(chrono::duration_cast<chrono::milliseconds>(current_time-last_anim_time).count()>step_time.getPayload()){
                        animation_state++;
                        last_anim_time=current_time;
                    }
                    if(animation_state==vertex_anims->size()){                    
                        animation_state=0;
                        is_animating=false;
                        aj->pathProcessed = true;
                    }                
                }
            }
            // Drawing left and right panel
            window.draw(leftPanel);
            window.draw(rightPanel);
            startBFSPathFinding.drawButton();
        }else if (active_scene == COMMUNITIES){ 
            // drawing graph
            drawGraphCommunities(&window,BUTTON_WIDTH,BUTTON_HEIGHT,aj);           
            // Drawing left and right panel
            window.draw(leftPanel);
            window.draw(rightPanel);
            // drawing communities button
            startCommunities.drawButton();
        }else { // active scene == PAGERANK
            // drawing graph
            drawGraphPageRank(&window,BUTTON_WIDTH,BUTTON_HEIGHT,aj); 
            // Drawing left and right panel
            window.draw(leftPanel);
            window.draw(rightPanel);
            // drawing pagerank button
            startPageRank.drawButton();
        }
        // Drawing top buttons
        for(int i =0; i < NB_BUTTONS ; i++){
            button_list[i]->drawButton();
        }
        // Drawing reset button
        resetView.drawButton();
        // Drawing generator
        for(auto i: text_boxs_int){
            i->drawTextBox();
        }
        for(auto i: text_boxs_float){
            i->drawTextBox();
        }
        for(auto i: text_boxs_string){
            i->drawTextBox();
        }
        generate.drawButton();
        generate_from_file.drawButton();
        is_directed.drawCheckBox();
        //Drawing lines
        window.draw(line_left, 2, sf::Lines);
        window.draw(line_right, 2, sf::Lines);
        
        if(selecting==true){
            float ratio = CANVAS_WIDTH/CANVAS_HEIGHT;
            //if out of canvas, cancelling selection
            
            auto testx = sf::Mouse::getPosition(window).x <= BUTTON_WIDTH*4 && sf::Mouse::getPosition(window).x >= BUTTON_WIDTH;
            auto testy = sf::Mouse::getPosition(window).y <= CANVAS_HEIGHT+BUTTON_HEIGHT && sf::Mouse::getPosition(window).y >= BUTTON_HEIGHT;
            if( !(testx && testy)){
                selecting = false;
            }else{
                // getting current position
                auto current_x = sf::Mouse::getPosition(window).x;
                auto current_y = sf::Mouse::getPosition(window).y;
                // getting absolute offset
                auto offset_x = abs(current_x-selection_x);
                auto offset_y = abs(current_y-selection_y);

                if(offset_x!=0 && offset_y!=0){
                    // getting end coordinate for the selection box
                    float end_x;
                    float end_y;
                    if(offset_x/offset_y>=ratio){
                        end_x = offset_x;                    
                        end_y = end_x/ratio;
                        
                    }else{
                        end_y = offset_y;                    
                        end_x = end_y*ratio;
                    }
                    float pos_x;
                    float pos_y;
                    if(current_x >= selection_x && current_y>=selection_y){
                        pos_x = selection_x;
                        pos_y = selection_y;
                    }else if(current_x >= selection_x && current_y<=selection_y){
                        pos_x = selection_x;
                        pos_y = selection_y-end_y;
                    }else if(current_x <= selection_x && current_y<=selection_y){
                        pos_x = selection_x-end_x;
                        pos_y = selection_y-end_y;
                    }else{
                        pos_x = selection_x-end_x;
                        pos_y = selection_y;
                    }
                    //drawing selection box sf::Color::Transparent
                    sf::RectangleShape selection_box(sf::Vector2f(end_x,end_y));
                    selection_box.setPosition(pos_x, pos_y);
                    selection_box.setFillColor(sf::Color::Transparent);
                    selection_box.setOutlineColor(sf::Color(255,0,255));
                    selection_box.setOutlineThickness(SELECTION_OUTLINE_WIDTH);
                    window.draw(selection_box);
                }
            }       
        }  

        window.display();
        
    }

    delete aj;
    delete node_anims;
    delete vertex_anims;
    delete wgarg;

    for(int i = 0 ;  i < NB_BUTTONS ; i++){
        delete button_list[i];
    }

    return 0;
}
