// ------------------------------------------------
// communities.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/communities.h"

void processing_communities(AdjArray* graph){
    bool go_on = true;
    unsigned int* shuffle = (unsigned int *)malloc(graph->n*sizeof(unsigned int));
    if(shuffle == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    for(int i = 0; i < graph->n ; i++){
        graph->labels[i]=i;
        shuffle[i]=i;
    }    
    while(go_on){
        //resetting our test
        go_on=false;
        //shuffling using the Fisher-Yates method
        for(int i = graph->n-1; i > 0 ; i--){
            int j = rand() % (i+1) ;
            auto tmp = shuffle[j];
            shuffle[j]=shuffle[i];
            shuffle[i]=tmp; 
        }
        for(int i = 0; i < graph->n;i++){
            auto focused_node = shuffle[i];
            int nb_neighbours = graph->cd[focused_node+1]-graph->cd[focused_node];
            if(nb_neighbours>1){
                // array of vector to stock the counts
                vector<unsigned long> arr[nb_neighbours];
                // getting the sorted list of neighbours' labels
                unsigned long* sorted_list = (unsigned long*)malloc(sizeof(unsigned long)*nb_neighbours);
                if(sorted_list == NULL){
                    cerr << "couldn't allocate memory, aborting" << endl;
                    exit(1);
                }
                //memcpy(sorted_list,&graph->adj[graph->cd[focused_node]],sizeof(unsigned long)*nb_neighbours);
                int j = 0;
                int k = graph->cd[focused_node];
                while( j < nb_neighbours ){
                    sorted_list[j] = graph->labels[graph->adj[k]];
                    j++;
                    k++;
                }
                sort(sorted_list,sorted_list+nb_neighbours);
                // getting the number of occurences
                
                int count = 1;
                int res = sorted_list[0];
                for (int m = 1; m < nb_neighbours; m++)
                {
                    if(sorted_list[m]==sorted_list[m-1]){
                        count ++; 
                        if(m==nb_neighbours-1){
                            arr[count-1].push_back(sorted_list[m-1]);
                        }                       
                    }else{
                        arr[count-1].push_back(sorted_list[m-1]);
                        count = 1;
                        if(m==nb_neighbours-1){
                            arr[0].push_back(sorted_list[m]);
                        } 
                    }                    
                }
                // getting array containing labels with most occurences
                int h = nb_neighbours-1;
                while(arr[h].size()==0){
                    h--;
                }
                //checking if among those labels, the node already has one
                //if so, not changing the label
                bool next = false;
                for(auto lab: arr[h]){
                    if(lab==graph->labels[focused_node]){
                        next = true;
                        break;
                    }
                }
                if(next){
                    continue;
                }
                graph->labels[focused_node] = arr[h][rand() % arr[h].size()];
                go_on = true;

                free(sorted_list);
                
            }else if(nb_neighbours==1){
                graph->labels[focused_node] = graph->adj[graph->cd[focused_node]];
            }
        }   
           
    }
    graph->communities_processed = true;
    graph->labels_colors.clear();
    for(int i=0; i < graph->n ; i++){
        if(graph->labels_colors.count(graph->labels[i])==0){
            graph->labels_colors[graph->labels[i]] = sf::Color(rand()%256,rand()%256,rand()%256);
        }
    }

    // freeing
    free(shuffle); 
}