// ------------------------------------------------
// 2dGeometry.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/2dGeometry.h"


Point::Point(float xarg, float yarg){
    x=xarg;
    y=yarg;
}

Point::Point(){
    x=0.0f;
    y=0.0f;
}
void Point::translate(Vector v){
    x += v.getVX();
    y += v.getVY();
}

void Point::setX(float xarg){
    x=xarg;
}
void Point::setY(float yarg){
    y=yarg;
}
float Point::getX(){
    return x;
}
float Point::getY(){
    return y;
}

Vector::Vector(float vxarg, float vyarg){
    vx=vxarg;
    vy=vyarg;
}
Vector::Vector(){
    vx=0.0f;
    vy=0.0f;
}

void Vector::translate(Vector v){
    vx += v.getVX();
    vy += v.getVY();
}

void Vector::setVX(float vxarg){
    vx=vxarg;
}
void Vector::setVY(float vyarg){
    vy=vyarg;
}
float Vector::getVX(){
    return vx;
}
float Vector::getVY(){
    return vy;
}

Vector Vector::operator*(float k){
    Vector ret(this->getVX()*k,this->getVY()*k);
    return ret;
}
Vector Vector::operator/(float k){
    Vector ret(this->getVX()/k,this->getVY()/k);
    return ret;
}


float Vector::getNorm(){
    return sqrt(vx*vx+vy*vy);
}

float distance(Point a, Point b){
    return sqrt(  (a.getX()-b.getX())*(a.getX()-b.getX())
                + (a.getY()-b.getY())*(a.getY()-b.getY()));
}

Vector vectorDirector(Point from, Point to){
    Vector ret(to.getX()-from.getX(),to.getY()-from.getY());
    return ret;
}