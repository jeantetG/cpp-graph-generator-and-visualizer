// ------------------------------------------------
// pathfinding.cpp
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/pathfinding.h"

bool leftNodeClick(float mx, float my, float originx, float originy, AdjArray* graph){
    int id_chosen = -1;
    for(int i =0; i < graph->n ; i ++){
        // change origin to the one of the canvas
        Point tmp(mx-originx,my-originy);
        // nodes position are on the top left corner of the circle
        // needs to be translated
        Point tmpnode(morphToScaleX(graph->positions[i].getX(), graph)+graph->delta_x+STD_NODE_SIZE_PX,morphToScaleY(graph->positions[i].getY(),graph)+graph->delta_y+STD_NODE_SIZE_PX);
        //getting distance
        if(distance(tmpnode, tmp) <= STD_NODE_SIZE_PX){
            id_chosen = i;
            break;
        }
    }
    if(id_chosen!=-1){
        if(graph->target!=-1){
            graph->node_type_list[graph->target] = NONE;
        }        
        graph->target = id_chosen;
        graph->node_type_list[graph->target] = TARGET;
        // check if target == start
        if(graph->target == graph->start){
            graph->start = -1;
        }
        // reset path draw
        graph->pathProcessed = false;

        return true;
    }
    return false;    
}

bool rightNodeClick(float mx, float my, float originx, float originy, AdjArray* graph){
    int id_chosen = -1;
    for(int i =0; i < graph->n ; i ++){
        // change origin to the one of the canvas
        Point tmp(mx-originx,my-originy);
        // nodes position are on the top left corner of the circle
        // needs to be translated
        Point tmpnode(morphToScaleX(graph->positions[i].getX(), graph)+graph->delta_x+STD_NODE_SIZE_PX,morphToScaleY(graph->positions[i].getY(),graph)+graph->delta_y+STD_NODE_SIZE_PX);
        //getting distance
        if(distance(tmpnode, tmp) <= STD_NODE_SIZE_PX){
            id_chosen = i;
            break;
        }
    }
    if(id_chosen!=-1){
        if(graph->start!=-1){
            graph->node_type_list[graph->start] = NONE;
        } 
        graph->start = id_chosen;
        graph->node_type_list[graph->start] = START;
        // check if target == start
        if(graph->target == graph->start){
            graph->target = -1;
        }
        // reset path draw
        graph->pathProcessed = false;
        return true;
    }
    return false;    
}

void shortestPathBFS(WGarg* wgarg){
    AdjArray* aj = wgarg->graph;
    // resetting animations
    wgarg->animations_node->clear();
    wgarg->animations_vertex->clear();
    // allocating array for keeping track of marked nodes
    bool* marked = (bool *) calloc(aj->n,sizeof(bool));
    if(marked == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    // resetting distance to origin
    for(int i =0; i < aj->n ; i++){
        aj->dist_to_origin[i] = -1;
    }
    // creating fifo queue
    FIFO fif(NLINKS);
    // adding starting node
    fif.push(aj->start);
    // marking starting node
    marked[aj->start] = true;
    
    aj->dist_to_origin[aj->start]=0;
    // creating stop guard
    bool stop = false;
    // starting loop
    while(fif.size()!=0){
        //getting first node
        int tmp = fif.pop();
        //iterating over the neighbours
        for(int i = aj->cd[tmp] ; i < aj->cd[tmp+1] ; i++){
            int art = aj->adj[i];
            // new distance
            unsigned long new_dist = aj->dist_to_origin[tmp] + 1;
            // if new distance is shorter, updating it
            if(new_dist<aj->dist_to_origin[art]){
                aj->dist_to_origin[art] = new_dist;
                aj->predecessor[art] = tmp;
            }
            // marking and pushing if not visited before
            if(marked[art] == false){
				fif.push(art);

				marked[art] = true;

                //Animation                
                sf::CircleShape node(STD_NODE_SIZE_PX);
                sf::Color col;
                if(dark_mode_on==true){
                    node.setOutlineThickness(STD_NODE_SIZE_PX/10);
                    node.setOutlineColor(sf::Color(240,240,240));
                    col = sf::Color(200, 200, 0);
                }else{
                    col = sf::Color(255, 255, 0);
                } 
                node.setFillColor(col);                             
                node.setPosition(aj->positions[art].getX(),aj->positions[art].getY());
                wgarg->animations_node->push_back(node);                    
                 
                //getting starting and ending points
                float a_x = aj->positions[tmp].getX() ;
                float a_y = aj->positions[tmp].getY() ;
                float b_x = aj->positions[art].getX() ;
                float b_y = aj->positions[art].getY() ;
                
                VertexArgs ret(a_x,a_y,b_x,b_y);
                wgarg->animations_vertex->push_back(ret);
                
        
                if(art==aj->target){
                    stop = true;
                    break;
                } 

			}
            
        }        
        if(stop){
            break;
        }
    }
    if(!stop){
        aj->pathProcessed = false;
        free(marked);
        return;
    }
    
    // stocking shortest path length if necessary later
    aj->shortest_path_length = aj->dist_to_origin[aj->target];
    // notifying that the path has been processed
    aj->pathProcessed = true;

    //freeing
    free(marked);

}