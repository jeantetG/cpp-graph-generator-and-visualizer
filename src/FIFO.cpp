// ------------------------------------------------
// FIFO.h
//
// Author: Gabriel Jeantet
// Email: gabriel.jeantetpro@gmail.com
// Date  : 04-03-2020
//
// ------------------------------------------------

#include "../include/FIFO.h"
#include <iostream>

using namespace std;

FIFO::FIFO(int cap){
    fifo = (int*) malloc(sizeof(int)*cap);
    if(fifo == NULL){
        cerr << "couldn't allocate memory, aborting" << endl;
        exit(1);
    }
    capacity = cap;
    sz = 0;
    first = 0;
    last = -1;
}
FIFO::~FIFO(){
    free(fifo);
}

int FIFO::pop(){
    if(this->isEmpty()){
        std::cerr << "FIFO empty can't pop"<< std::endl;
        exit(-1);
    }
    //std::cout << "popping" << std::endl;
    int tmp = first;
    first = (first+1)%capacity;
    sz--;
    return fifo[tmp];
}
void FIFO::push(int t){
    if(this->isFull()){
        std::cerr << "FIFO full can't push"<< std::endl;
        exit(-1);
    }
    //std::cout << "pushing " << std::endl;
    last = (last+1) % capacity;
    fifo[last] = t;
    sz++;

}

bool FIFO::isEmpty(){
    return sz == 0;
}

bool FIFO::isFull(){

    return sz==capacity;
}

int FIFO::size(){
    return sz;
}